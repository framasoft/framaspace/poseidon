{% from "framaspace/map.jinja" import hosts with context %}
rsyslog-relp:
  pkg.installed:
    - name: rsyslog-relp
  file.managed:
    - name: /etc/rsyslog.d/00-imrelp.conf
    - source: salt://framaspace/syslog/files/imrelp.conf.sls
    - template: jinja
    - default:
      ip: {{ hosts[grains['id']]['ip'] }}

/etc/rsyslog.d/10-cluster.conf:
  file.managed:
    - source: salt://framaspace/syslog/files/cluster.conf.sls
    - template: jinja
    - default:
      domain: {{ pillar['framaspace']['domain'] }}

/etc/rsyslog.d/99-nextcloud-catchall.conf:
  file.managed:
    - source: salt://framaspace/syslog/files/catchall.conf

dependency:
  file.managed:
    - name: /etc/systemd/system/rsyslog.service.d/override.conf
    - makedirs: True
    - contents:
      - '[Unit]'
      - Requires=wg-quick@framaspace.service
      - After=wg-quick@framaspace.service
      - '[Service]'
      - ExecStartPre=/usr/bin/perl -e 'sleep 1 until -e "/sys/class/net/framaspace"'
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/rsyslog.service.d/override.conf

reload-rsyslog:
  service.running:
    - name: rsyslog
    - reload: False
    - enable: True
    - watch:
      - pkg: rsyslog-relp
      - file: rsyslog-relp
      - file: /etc/rsyslog.d/10-cluster.conf
      - file: /etc/rsyslog.d/99-nextcloud-catchall.conf
    - onlyif:
      - /usr/sbin/rsyslogd -N1
