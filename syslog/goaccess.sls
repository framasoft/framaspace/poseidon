goaccess:
  pkg.installed:
    - name: goaccess
  file.append:
    - name: /etc/goaccess/goaccess.conf
    - text:
      - 'time-format %H:%M:%S'
      - 'date-format %d/%b/%Y'
      - 'log-format %^ %^ %^ %v %h %^ %^ [%d:%t %^] "%r" %s %b "%R" "%u"'
