# vim:set ft=conf
# Choisir un chemin de log d'apres le nom du programme
$template programname,"/var/log/cluster/%programname%/%programname%.log"

# Recuperer le 2e champ, avec separation des champs par des espaces
# (32 est la valeur US-ASCII d'une espace)
# https://www.rsyslog.com/how-to-use-set-variable-and-exec_template/
# https://www.rsyslog.com/doc/v8-stable/configuration/property_replacer.html
template(name="extract_host"              type="string" string="%msg:F,32:2%")
template(name="extract_first_letter_host" type="string" string="%msg:F,32,1:2,1%")
template(name="extract_two_letters_host"  type="string" string="%msg:F,32,1:2,2%")

## Nginx
if $programname == 'nginx' then {
  # Recuperation du nom de domaine (en-tete host)
  set $!a = exec_template("extract_first_letter_host");
  set $!b = exec_template("extract_two_letters_host");
  set $!c = exec_template("extract_host");
  $template alllog,"/var/log/cluster/web-all-spaces/all-spaces.access.log"
  $template accesslog,"/var/log/cluster/web/%$!a%/%$!b%/%$!c%/nginx.access.log"
  $template fspaceaccesslog,"/var/log/cluster/web/{{ domain }}/nginx.access.log"

  # On regarde si on peut recuperer le nom de domaine dans le log (d'erreur, pour le coup)
  # https://www.rsyslog.com/doc/v8-stable/rainerscript/functions/rs-re_extract.html
  set $!d = re_extract($msg, 'host: "([^"])[^"]*{{ domain }}"', 0, 1, "");
  set $!e = re_extract($msg, 'host: "([^"][^"])[^"]*{{ domain }}"', 0, 1, "");
  set $!f = re_extract($msg, 'host: "([^"]*{{ domain }})"', 0, 1, "");
  $template errorlog,"/var/log/cluster/web/%$!d%/%$!e%/%$!f%/nginx.error.log"
  $template fspaceerrorlog,"/var/log/cluster/web/{{ domain }}/nginx.error.log"

  $template nohostlog,"/var/log/cluster/nginx/no-host.log"

  action(type="omfile" dynaFile="alllog")
  if $!c == '{{ domain }}' then {
      action(type="omfile" dynaFile="fspaceaccesslog")
  } else if $!f == '{{ domain }}' then {
      action(type="omfile" dynaFile="fspaceerrorlog")
  } else if $!c contains('{{ domain }}') then {
      action(type="omfile" dynaFile="accesslog")
  } else if $!f contains('{{ domain }}') then {
      action(type="omfile" dynaFile="errorlog")
  } else {
      action(type="omfile" dynaFile="nohostlog")
  }
  stop
} else

## Haproxy
if $programname == 'haproxy' then {
  # On regarde si on peut recuperer le nom de domaine dans le log
  set $!h = re_extract($msg, '"([^"])[^"]*{{ domain }}"', 0, 1, "");
  set $!i = re_extract($msg, '"([^"][^"])[^"]*{{ domain }}"', 0, 1, "");
  set $!j = re_extract($msg, '"([^"]*{{ domain }})"', 0, 1, "");
  $template haproxylog,"/var/log/cluster/web/%$!h%/%$!i%/%$!j%/haproxy.log"
  $template fspacehaproxylog,"/var/log/cluster/web/%!j%/haproxy.log"
  $template miniohaproxylog,"/var/log/cluster/minio/%!j%.access.log"

  $template nohostlog,"/var/log/cluster/haproxy/no-host.haproxy.log"

  if $!j == '{{ domain }}' then {
      action(type="omfile" dynaFile="fspacehaproxylog")
  } else if $!j == 'minio.{{ domain }}' then {
      action(type="omfile" dynaFile="miniohaproxylog")
  } else if $!j == 'minio-admin.{{ domain }}' then {
      action(type="omfile" dynaFile="miniohaproxylog")
  } else if $!j == 'minio-rep.{{ domain }}' then {
      action(type="omfile" dynaFile="miniohaproxylog")
  } else if $!j == 'minio-rep-admin.{{ domain }}' then {
      action(type="omfile" dynaFile="miniohaproxylog")
  } else if $!j contains('{{ domain }}') then {
      action(type="omfile" dynaFile="haproxylog")
  } else {
      action(type="omfile" dynaFile="nohostlog")
  }
  stop
} else

## Redis
if $programname == 'redis' then {
  action(type="omfile" dynaFile="programname")
  stop
} else

## PostgreSQL
if $programname == 'postgres' then {
  action(type="omfile" dynaFile="programname")
  stop
} else

## Minio
if $programname == 'minio' then {
  $template replog,"/var/log/cluster/%programname%/%programname%-rep.log"
  if $hostname startswith 'minio-rep' then {
    action(type="omfile" dynaFile="replog")
  } else {
    action(type="omfile" dynaFile="programname")
  }
  stop
} else

## Sidekick
if $programname startswith 'minio-sidekick-' then {
  $template sidekicklog,"/var/log/cluster/sidekick/%hostname%-%programname%.log"
  action(type="omfile" dynaFile="sidekicklog")
  stop
} else

## Pgpool
if $programname == 'pgpool' then {
  action(type="omfile" dynaFile="programname")
  stop
} else

## Chronos
if $programname == 'chronos' then {
  action(type="omfile" dynaFile="programname")
  stop
}

## Notify-push
if $programname == 'notify-push' then {
  action(type="omfile" dynaFile="programname")
  stop
}
