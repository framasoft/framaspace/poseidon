/etc/logrotate.d/framaspace:
  pkg.installed:
    - name: pbzip2
  file.managed:
    - source: salt://framaspace/syslog/files/logrotate.framaspace
