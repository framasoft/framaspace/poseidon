{% set db = pillar['framaspace']['chronos']['db'] %}
chronos-db:
  postgres_user.present:
    - name: {{ db['user'] }}
    - password: {{ db['password'] }}
  postgres_database.present:
    - name: {{ db['name'] }}
    - owner: {{ db['user'] }}
