{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-create'] %}
{% if grains['id'] == f['pg']['primary'] %}
##########################################
## Create
##########################################
{%   for instance in j.keys() %}
{%       set pwd = '%s-%s' % (instance, f['pg']['password-seed']) %}
create-{{ instance }}:
  postgres_user.present:
    - name: '{{ instance }}'
    - password: {{ pwd | sha256 }}
  postgres_database.present:
    - name: {{ 'nc-%s' % instance }}
    - owner: '{{ instance }}'
{%   endfor %}
{% endif %}
