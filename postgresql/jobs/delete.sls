{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-delete'] %}
{% if grains['id'] == f['pg']['primary'] %}
##########################################
## Delete
##########################################
{%   for instance in j.keys() %}
delete-{{ instance }}:
  postgres_database.absent:
    - name: {{ 'nc-%s' % instance }}
  postgres_user.absent:
    - name: {{ instance }}
{%   endfor %}
{% endif %}
