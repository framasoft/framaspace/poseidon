{% from "framaspace/map.jinja" import hosts with context %}
{% set barman_server           = pillar['hosts'][pillar['barman']['server']] %}
{% set data                    = pillar['hosts'][grains['id']] %}
{% if 'current_pg_version' in pillar %}
{%   set pg_major_version      = pillar['current_pg_version'] %}
{% else %}
{%   set pg_major_version      = salt['pkg.available_version']('postgresql').split('+')[0] or salt['pkg.version']('postgresql').split('+')[0] %}
{% endif %}
{% set hba_nginx_php_servers   = [] %}
{% set hba_notify_push_servers = [] %}
{% set hba_pg_servers          = [] %}
{% set pg_servers_names        = [] %}
{% for hostname, value in hosts.items()|sort %}
{%   if hostname is match('^nginx') %}
{%     do  hba_nginx_php_servers.append('host    all             all             %s/32            md5' % value['ip']) %}
{%     do  hba_nginx_php_servers.append('host    postgres        pgpool_check    %s/32            md5' % value['ip']) %}
{%   elif hostname is match('^notify-push') %}
{%     do  hba_notify_push_servers.append('host    all             all             %s/32            md5' % value['ip']) %}
{%     do  hba_notify_push_servers.append('host    postgres        pgpool_check    %s/32            md5' % value['ip']) %}
{%   elif hostname is match('^postgresql') and hostname != grains['id'] %}
{%     do   hba_pg_servers.append('host    replication     fspace_rep_user %s/32            md5' % value['ip']) %}
{%     do pg_servers_names.append(hostname) %}
{%   elif hostname is match('^postgresql') and hostname == grains['id'] %}
{%     do   hba_pg_servers.append('host    all             all             %s/32            md5' % value['ip']) %}
{%   endif %}
{% endfor %}

configure_cluster_access:
  file.blockreplace:
    - name: /etc/postgresql/{{ pg_major_version }}/main/pg_hba.conf
    - marker_start: 'host    all             all             ::1/128                 md5'
    - marker_end: '# Allow replication connections from localhost'
    - content: "# Nginx/PHP servers\n{{ hba_nginx_php_servers | join('\\n') }}\n# Notify_push servers\n{{ hba_notify_push_servers | join('\\n')}}\n# PostgreSQL servers\n{{ hba_pg_servers | join('\\n') }}"

reload-pg-{{ grains['id'] }}:
  service.running:
    - name: postgresql
    - enable: True
    - reload: True
    - watch:
      - file: /etc/postgresql/{{ pg_major_version }}/main/pg_hba.conf
