include:
  - framaspace.common.rsyslog
  - .postgresql
{% if grains['id'] == pillar['framaspace']['pg']['primary'] %}
  - .instances
  - .chronos
{% else %}
  - .synchronize-replica
{% endif %}
