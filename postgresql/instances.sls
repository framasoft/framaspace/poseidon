{% set pg       = pillar['framaspace']['pg'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% do active.update(inactive) %}
{% if grains['id'] == pg['primary'] %}
{%   for instance in active.keys() %}
{%     set pwd = '%s-%s' % (instance, pg['password-seed']) %}
{{ instance }}:
  postgres_user.present:
    - password: {{ pwd | sha256 }}
  postgres_database.present:
    - name: {{ 'nc-%s' % instance }}
    - owner: {{ instance }}
{%   endfor %}
{% endif %}
