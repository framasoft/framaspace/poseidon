{% from "framaspace/map.jinja" import hosts with context %}
{% set pg             = pillar['framaspace']['pg'] %}
{% set already_synced = salt['file.file_exists']('/etc/postgresql/already-synced-from-%s' % pg['primary']) %}
{% if grains['id'] is match('^postgresql-')
      and grains['id'] != pg['primary']
      and pillar['hosts'][grains['id']]['framaspace'] is defined
      and pillar['hosts'][grains['id']]['framaspace']
      and not already_synced %}

{% set pg_major_version = salt['pkg.available_version']('postgresql').split('+')[0] or salt['pkg.version']('postgresql').split('+')[0] %}

stop-service-postgresql:
  service.dead:
    - name: postgresql
mask-service-postgresql:
  service.masked:
    - name: postgresql

clear-out-postgres-data-files:
  cmd.run:
    - name: rm -rf /var/lib/postgresql/{{ pg_major_version }}/main/*
    - runas: postgres
    - require:
      - service: stop-service-postgresql
      - service: mask-service-postgresql

clear-out-pgwal-files:
  cmd.run:
    - name: rm -rf /var/lib/postgresql/pg{{ pg_major_version }}_wal/*
    - runas: postgres
    - require:
      - service: stop-service-postgresql
      - service: mask-service-postgresql

stream-basebackup-from-master:
  cmd.run:
    - name: |
        /usr/bin/pg_basebackup \
          -h {{ hosts[pg['primary']]['ip'] }} \
          -p 5432 \
          -U {{ pg['replication-user'] }} \
          --pgdata /var/lib/postgresql/{{ pg_major_version }}/main/ \
          --format=plain \
          --wal-method=stream \
          --checkpoint fast \
          --no-password
          #--write-recovery-conf \
    - runas: postgres
    - require:
      - cmd: clear-out-postgres-data-files
      - cmd: clear-out-pgwal-files
  file.managed:
    - name: /var/lib/postgresql/{{ pg_major_version }}/main/standby.signal


unmask-postgresql:
  service.unmasked:
    - name: postgresql

start-postgresql:
  service.running:
    - name: postgresql
    - enable: True

/etc/postgresql/already-synced-from-{{ pg['primary'] }}:
  file.managed:
    - user: root

{% endif %}
