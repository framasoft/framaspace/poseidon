{% from "framaspace/map.jinja" import hosts with context %}
{% set barman_server           = pillar['hosts'][pillar['barman']['server']] %}
{% set data                    = pillar['hosts'][grains['id']] %}
{% set pg_major_version        = salt['pkg.available_version']('postgresql').split('+')[0] or salt['pkg.version']('postgresql').split('+')[0] %}
{% set pg                      = pillar['framaspace']['pg'] %}
{% set hba_nginx_php_servers   = [] %}
{% set hba_notify_push_servers = [] %}
{% set hba_pg_servers          = [] %}
{% set pg_servers_names        = [] %}
{% for hostname, value in hosts.items()|sort %}
{%   if hostname is match('^nginx') %}
{%     do  hba_nginx_php_servers.append('host    all             all             %s/32            md5' % value['ip']) %}
{%     do  hba_nginx_php_servers.append('host    postgres        pgpool_check    %s/32            md5' % value['ip']) %}
{%   elif hostname is match('^notify-push') %}
{%     do  hba_notify_push_servers.append('host    all             all             %s/32            md5' % value['ip']) %}
{%     do  hba_notify_push_servers.append('host    postgres        pgpool_check    %s/32            md5' % value['ip']) %}
{%   elif hostname is match('^postgresql') and hostname != grains['id'] %}
{%     do   hba_pg_servers.append('host    replication     fspace_rep_user %s/32            md5' % value['ip']) %}
{%     do pg_servers_names.append(hostname) %}
{%   elif hostname is match('^postgresql') and hostname == grains['id'] %}
{%     do   hba_pg_servers.append('host    all             all             %s/32            md5' % value['ip']) %}
{%   endif %}
{% endfor %}

# Thanks to https://www.corywright.org/blog/posts/postgresql-12-replication-with-saltstack/

postgresql-{{ grains['id'] }}-installation:
  pkg.installed:
    - pkgs:
      - postgresql
      - libdbd-pg-perl

/opt/wait-interfaces.sh:
  file.managed:
    - mode: 755
    - contents:
      - '#!/bin/bash'
{% if grains['id'] == pg['primary'] %}
      - while [[ ! -e /sys/class/net/ens13 ]]; do
      - '    sleep 1'
      - done
{% endif %}
      - while [[ ! -e /sys/class/net/framaspace ]]; do
      - '    sleep 1'
      - done

dependency:
  file.managed:
    - name: /etc/systemd/system/postgresql.service.d/override.conf
    - makedirs: True
    - contents:
      - '[Unit]'
      - Requires=wg-quick@framaspace.service
      - After=wg-quick@framaspace.service
      - Requires=sys-devices-virtual-net-framaspace.device
      - After=sys-devices-virtual-net-framaspace.device
      - Requires=network-online.target
      - After=network-online.target
      - '[Service]'
      - ExecStartPre=/opt/wait-interfaces.sh
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/postgresql.service.d/override.conf

postgres_alias_{{ grains['id'] }}:
  alias.present:
    - name: postgres
    - target: root
    - watch:
      - pkg: postgresql-{{ grains['id'] }}-installation
postgresql-{{ grains['id'] }}-psqlrc:
  file.append:
    - name: /var/lib/postgresql/.psqlrc
    - text:
      - \x auto
postgresql-confd-{{ grains['id'] }}:
  file.uncomment:
    - name: /etc/postgresql/{{ pg_major_version }}/main/postgresql.conf
    - regex: include_dir =
pghba-{{ grains['id'] }}:
  file.append:
    - name: /etc/postgresql/{{ pg_major_version }}/main/pg_hba.conf
    - text:
      - '# Backup'
{% for ip in barman_server['ip'] %}
      - 'hostssl all             barman           {{ ip }}/32      md5'
      - 'hostssl replication     streaming_barman {{ ip }}/32      md5'
{% endfor -%}
{% for ip in barman_server['ipv6'] %}
      - 'hostssl all             barman           {{ ip }}/128      md5'
      - 'hostssl replication     streaming_barman {{ ip }}/128      md5'
{% endfor %}
postgresql-wal_dir-{{ grains['id'] }}:
  file.directory:
    - name: /var/lib/postgresql/pg{{ pg_major_version }}_wal/
    - user: postgres
    - group: postgres
    - mode: 0750
postgresql-conf-{{ grains['id'] }}:
  file.managed:
    - name: /etc/postgresql/{{ pg_major_version }}/main/conf.d/postgresql.conf
    - contents:
      - listen_addresses = '{% if grains['id'] == pg['primary'] -%}*{%- else -%}{{ hosts[grains['id']]['ip'] }}{%- endif %}'
      - wal_level = 'replica'
      - wal_log_hints = on
      - hot_standby = 'on'
      - log_destination = 'syslog,stderr'
      - cluster_name = '{{ grains['id'] | replace('-', '') }}'
{% if grains['id'] == pg['primary'] %}
      - synchronous_commit = remote_apply
      - '# https://www.crunchydata.com/blog/synchronous-replication-in-postgresql'
      - synchronous_standby_names = '{{ pg_servers_names | join(',') | replace('-', '') }}'
{% else %}
      - recovery_target_timeline = latest
      - primary_conninfo = 'user=fspace_rep_user passfile=''/var/lib/postgresql/.pgpass'' channel_binding=prefer host={{ hosts[pg['primary']]['ip'] }} port=5432 sslmode=prefer sslcompression=0 ssl_min_protocol_version=TLSv1.2 gssencmode=prefer krbsrvname=postgres target_session_attrs=any'
{% endif %}
      - '# Common config'
{% for key, val in pg['config'].items() %}
      - {{ key }} = {{ val }}
{% endfor %}
{% if grains['id'] == pg['primary'] %}
      - '# Primary config'
{%   for key, val in pg['primary_config'].items() %}
      - {{ key }} = {{ val }}
{%   endfor %}
{% else %}
      - '# Secondary config'
{%   for key, val in pg['secondary_config'].items() %}
      - {{ key }} = {{ val }}
{%   endfor %}
{% endif %}

configure_cluster_access:
  file.blockreplace:
    - name: /etc/postgresql/{{ pg_major_version }}/main/pg_hba.conf
    - marker_start: 'host    all             all             ::1/128                 md5'
    - marker_end: '# Allow replication connections from localhost'
    - content: "# Nginx/PHP servers\n{{ hba_nginx_php_servers | join('\\n') }}\n# Notify_push servers\n{{ hba_notify_push_servers | join('\\n')}}\n# PostgreSQL servers\n{{ hba_pg_servers | join('\\n') }}"

restart-pg-{{ grains['id'] }}:
  service.running:
    - name: postgresql
    - enable: True
    - reload: False
    - watch:
      - file: /etc/postgresql/{{ pg_major_version }}/main/conf.d/postgresql.conf

reload-pg-{{ grains['id'] }}:
  service.running:
    - name: postgresql
    - enable: True
    - reload: True
    - watch:
      - file: /etc/postgresql/{{ pg_major_version }}/main/pg_hba.conf

{% if grains['id'] == pg['primary'] %}
{{ pg['replication-user'] }}:
  postgres_user.present:
    - replication: True
    - password: {{ pg['replication-user-password'] }}

{{ pg['pgpool']['user'] }}:
  postgres_user.present:
    - password: {{ pg['pgpool']['pwd'] }}
{% endif %}

{% if data['pg'] is defined %}
barman:
  postgres_user.present:
    - superuser: True
    - password: {{ data['pg']['barman'] }}
streaming_barman:
  postgres_user.present:
    - replication: True
    - password: {{ data['pg']['streaming_barman'] }}
{% endif %}

/var/lib/postgresql/.pgpass:
  file.managed:
    - user: postgres
    - group: postgres
    - mode: 600
    - contents:
{% for hostname in pg_servers_names %}
      - '{{ hosts[hostname]['ip'] }}:5432:*:{{ pg['replication-user'] }}:{{ pg['replication-user-password'] }}'
{% endfor %}

