daily-cron-script:
  file.managed:
    - name: /var/www/paheko/production/src/scripts/factory_cron.sh
    - source: salt://framaspace/paheko/files/factory_cron.sh
    - mode: 740
    - user: www-data
    - group: www-data
  cron.present:
    - name: /var/www/paheko/production/src/scripts/factory_cron.sh
    - user: www-data
    - minute: 37
    - hour: 5
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - comment: 'Envoi des rappels de cotisation, sauvegardes, etc.'

email-cron-script:
  file.managed:
    - name: /var/www/paheko/production/src/scripts/factory_cron_emails.sh
    - source: salt://framaspace/paheko/files/factory_cron_emails.sh
    - mode: 740
    - user: www-data
    - group: www-data
  cron.present:
    - name: /var/www/paheko/production/src/scripts/factory_cron_emails.sh
    - user: www-data
    - minute: '*/10'
    - hour: '*'
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - comment: 'Envoi des mails en file d’attente'
