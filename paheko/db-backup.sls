# Backup is done by borgmatic, which triggers this script
db-backup-script:
  file.managed:
    - name: /opt/backup-all-paheko-databases.sh
    - source: salt://framaspace/paheko/files/backup-all-paheko-databases.sh
    - mode: 740
    - user: root
    - group: root
