#!/bin/bash

set -euo pipefail

# Répertoire où sont stockées les données des utilisateurs
# veiller à ce que ce soit le même que dans config.local.php
FACTORY_USER_DIRECTORY="/var/www/paheko-data/instances"

while IFS= read -r -d '' instance_path; do
    PAHEKO_FACTORY_USER=$(basename "$instance_path")
    export PAHEKO_FACTORY_USER
    /var/www/paheko/production/src/bin/paheko cron ||
        echo "Problem running paheko cron for instance $PAHEKO_FACTORY_USER"
done <  <(find "$FACTORY_USER_DIRECTORY" -mindepth 1 -maxdepth 1 -type d -print0)
