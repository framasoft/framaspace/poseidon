#!/bin/bash

set -euo pipefail

# Répertoire où sont stockées les données des utilisateurs
# veiller à ce que ce soit le même que dans config.local.php
INSTANCES_DIR="/var/www/paheko-data/instances"

FACTORY_BACKUP_DIR="/var/www/paheko-data/databases-backup"

while IFS= read -r -d '' instance_path; do
    instance_name=$(basename "$instance_path")
    export instance_name
    instance_backup_dir="$FACTORY_BACKUP_DIR/${instance_name:0:1}/${instance_name:0:2}"
    if [[ ! -d $instance_backup_dir ]]; then
        mkdir -p "$instance_backup_dir"
    fi
    # We need to remove the previous backup file or sqlite3 will bail out.
    if [[ -e "$instance_backup_dir/$instance_name.sqlite" ]]; then
        rm "$instance_backup_dir/$instance_name.sqlite"
    fi
    sqlite3 "$instance_path/association.sqlite" "VACUUM INTO '$instance_backup_dir/$instance_name.sqlite'" ||
        echo "Problem backuping database of instance $instance_name"
done <  <(find "$INSTANCES_DIR" -mindepth 1 -maxdepth 1 -type d -print0)
