<?php

namespace Paheko;

const API_USER = '{{ api_user }}';
const API_PASSWORD = '{{ api_pwd }}';

const NC_AUTH_SHARED_KEY = '{{ nc_shared_key }}';
const NC_HOST = '{{ instance }}.{{ domain }}';

const CSP_FRAME_ANCESTORS = '{{ instance }}.{{ domain }}';

const MAIL_RETURN_PATH = 'paheko-noreply+framaspace-{{ instance | string | truncate(37, True, '', 0) }}@{{ domain }}';
const MAIL_SENDER = 'paheko-noreply+framaspace-{{ instance | string | truncate(37, True, '', 0) }}@{{ domain }}';
