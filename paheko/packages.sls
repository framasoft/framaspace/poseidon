paheko-deps:
  pkg.installed:
    - pkgs:
      - composer
      - make
      - php-sqlite3
      - php8.2-sqlite3
      - php-zip
      - php8.2-zip
      - sqlite3
      - unzip
      - weasyprint
      - wget
