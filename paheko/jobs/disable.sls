{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-disable'] %}

{% for i, data in j.items() %}
{%   if 'paheko_shared_server' in data and data['paheko_shared_server'] == grains['id'] %}
disable-config-paheko-{{ i }}:
  file.rename:
    - name: /var/www/paheko-data/disabled-instances/{{ i }}
    - source: /var/www/paheko-data/instances/{{ i }}
    - force: True
    - makedirs: True
{%   else %}
not-the-good-server-{{ i }}:
  cmd.run:
    - name: echo "This is not the server you're looking for."
{%   endif %}
{% endfor %}
