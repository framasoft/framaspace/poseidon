{% set f = pillar['framaspace'] %}
{% set p = f['paheko'] | default({}) %}
{% set j = pillar['framaspace-jobs-create'] %}

{% for i, data in j.items() %}
{%   if 'paheko_shared_server' in data and data['paheko_shared_server'] == grains['id'] %}
dir-{{ i }}:
  file.directory:
    - name: /var/www/paheko-data/instances/{{ i }}
    - mode: 750
    - makedirs: True
    - user: www-data
    - group: www-data
config-{{ i }}:
  file.managed:
    - name: /var/www/paheko-data/config/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.paheko.{{ f['domain']}}/config.local.php
    - source: salt://framaspace/paheko/files/config.instance.php.sls
    - makedirs: True
    - user: www-data
    - group: www-data
    - template: jinja
    - default:
      api_user: {{ data['paheko_api_user'] }}
      api_pwd: {{ data['paheko_api_password'] }}
      nc_shared_key: {{ data['paheko_shared_secret_key'] }}
      instance: {{ i }}
      domain: {{ f['domain'] }}

{% set org = i[0] | upper + i[1:] %}
{% set pwd = '%s-%s' % (i, p['useless-password-seed']) %}
init-{{ i }}:
  cmd.run:
    - name: ./bin/paheko init --country FR --orgname {{ org }} --name {{ org }} --email 'paheko-noreply+framaspace-{{ i }}@{{ f['domain'] }}' --password {{ pwd | sha256 }}
    - cwd: /var/www/paheko/production/src
    - runas: www-data
    - require:
      - file: dir-{{ i }}
      - file: config-{{ i }}
    - env:
      - PAHEKO_FACTORY_USER: {{ i }}
{%   else %}
not-the-good-server-{{ i }}:
  cmd.run:
    - name: echo "This is not the server you're looking for."
{%   endif %}
{% endfor %}
