{% set f = pillar['framaspace'] %}
{% set p = f['paheko'] | default({}) %}
{% set j = pillar['framaspace-jobs-delete'] %}

{% for i, data in j.items() %}
{%   if 'paheko_shared_server' in data and data['paheko_shared_server'] == grains['id'] %}
delete-config-{{ i }}:
  file.absent:
    - name: /var/www/paheko-data/config/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.paheko.{{ f['domain']}}
delete-dir-{{ i }}:
  file.absent:
    - name: /var/www/paheko-data/disabled-instances/{{ i }}
{%   else %}
not-the-good-server-{{ i }}:
  cmd.run:
    - name: echo "This is not the server you're looking for."
{%   endif %}
{% endfor %}
