/var/www/paheko:
  file.directory:
    - user: www-data
    - group: www-data
/var/www/paheko-data/cache/web:
  file.directory:
    - mode: 750
    - makedirs: True
    - user: www-data
    - group: www-data
/var/www/paheko-data/config:
  file.directory:
    - mode: 750
    - makedirs: True
    - user: www-data
    - group: www-data
/var/www/paheko-data/databases-backup:
  file.directory:
    - mode: 750
    - makedirs: True
    - user: root
    - group: root
/var/www/paheko-data/disabled-instances:
  file.directory:
    - mode: 750
    - makedirs: True
    - user: www-data
    - group: www-data
/var/www/paheko-data/instances:
  file.directory:
    - mode: 750
    - makedirs: True
    - user: www-data
    - group: www-data
