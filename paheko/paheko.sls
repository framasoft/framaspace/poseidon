{% set f = pillar['framaspace'] %}
{% set p = f['paheko'] | default({}) %}
paheko:
  git.latest:
    - name: https://framagit.org/framasoft/framaspace/paheko.git
    - user: www-data
    - target: /var/www/paheko/production
    - branch: fspace
{% if 'version' in p %}
    - rev: {{ p['version'] }}
{% endif %}

paheko-deps:
  cmd.run:
    - name: make deps
    - cwd: /var/www/paheko/production/src
    - runas: www-data
    - require:
      - git: paheko

paheko-composer-deps:
  cmd.run:
    - name: composer install
    - cwd: /var/www/paheko/production/src
    - runas: www-data
    - require:
      - git: paheko

paheko-cache-link:
  file.symlink:
    - name: /var/www/paheko/production/src/www/.cache
    - target: /var/www/paheko-data/cache/web
    - user: www-data
    - group: www-data

{% set secret_key = '%s-%s' % (grains['id'], p['secret-key-seed']) %}
paheko-config:
  file.managed:
    - name: /var/www/paheko/production/src/config.local.php
    - source: salt://framaspace/paheko/files/config.local.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      domain: {{ f['domain'] }}
      secret_key: {{ secret_key | sha256 }}
