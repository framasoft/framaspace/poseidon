#!/bin/bash
for i in $(find /etc/letsencrypt/live/ -mindepth 1 -maxdepth 1 -type d)
do
    cd $i
    cat privkey.pem fullchain.pem > haproxy.pem
done
