frontend framaspace
  bind            :::80 v4v6
  bind            :::443 v4v6 ssl crt /etc/letsencrypt/live/principal-domain/haproxy.pem crt /etc/letsencrypt/live/principal-domain-wildcard/haproxy.pem crt /etc/letsencrypt/live/office-wildcard/haproxy.pem crt /etc/letsencrypt/live/principal-domain-wildcard/haproxy.pem crt /etc/letsencrypt/live/paheko-wildcard/haproxy.pem crt /etc/letsencrypt/live/alternative-domains/haproxy.pem crt /etc/letsencrypt/live/alternative-domains-wildcard/haproxy.pem ciphers ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-SHA256 alpn h2,http/1.1

  # Redirection vers HTTPS
  http-request    redirect code 301 scheme https code 301 if !{ ssl_fc }

  # HSTS
  http-after-response set-header Strict-Transport-Security "max-age=31536000; preload;"

  # Redirection vers le domaine {{ domain }}
  acl             alt-point hdr_end(host) -i -f /etc/haproxy/maps/point_alternative_domains.txt
  http-request    redirect code 301 location https://%[req.hdr(host),lower,word(1,.)].{{ domain }}%[capture.req.uri] if alt-point

  acl             alt-main  hdr(host)     -i -f /etc/haproxy/maps/alternative_domains.txt
  http-request    redirect code 301 location https://{{ domain }}%[capture.req.uri] if alt-main

  # Redirection vers la page d’accueil
  acl             root_domain hdr(host) -i {{ domain }}
  http-request    redirect code 301 location https://www.{{ domain }}%[capture.req.uri] if root_domain

  # Redirection vers la page "Space disabled because it was inactive"
  acl             space_disabled_because_inactive hdr(host) -i -f /etc/haproxy/maps/space_disabled_because_inactive.txt
  http-request    redirect code 307 location https://www.{{ domain }}/abc/disabled/inactivity/ if space_disabled_because_inactive

  # Bloc d’IPs de Framasoft
  # IP fixes des admins
  # IP des VPN
  # IP des machines de Framaspace
  acl             ips_framasoft     src -f /etc/haproxy/maps/ips_framasoft.txt

  # ACL pour les buckets minio (+ admin avec mc depuis nos machines)
  # (pas d’acces en direct, ce sont les NC qui contactent le cluster)
  acl             minio             hdr(host) -i minio.{{ domain }}
  http-request    deny              if minio !ips_framasoft

  # ACL pour l’admin de la replication minio avec mc depuis nos machines
  acl             minio-rep         hdr(host) -i minio-rep.{{ domain }}
  http-request    deny              if minio-rep !ips_framasoft

  # ACL console d’admin Minio
  acl             minio-admin       hdr(host) -i minio-admin.{{ domain }}
  http-request    deny              if minio-admin !ips_framasoft

  # ACL console d’admin Minio pour la replication
  acl             minio-rep-admin   hdr(host) -i minio-rep-admin.{{ domain }}
  http-request    deny              if minio-rep-admin !ips_framasoft

  # ACL imaginary
  acl             imaginary         hdr(host) -i imaginary.{{ domain }}

  # ACL des paheko
  acl             paheko            hdr(host) -i -m end .paheko.{{ domain }}

  # Choix du backend
  use_backend     minio             if minio
  use_backend     minio-rep         if minio-rep
  use_backend     minio-admin       if minio-admin
  use_backend     minio-rep-admin   if minio-rep-admin
  use_backend     imaginary_servers if imaginary
  # Utilisation d’une map, sans valeur par défaut
  use_backend     %[req.hdr(host),lower,map(/etc/haproxy/maps/paheko.map)] if paheko
  # Utilisation d’une map, avec nginx_servers en valeur par defaut
  # voir /etc/haproxy/maps/high-performance-backends.map
  # use_backend     %[req.hdr(host),lower,map(/etc/haproxy/maps/high-performance-backends.map,nginx_servers)] if { path_beg /push/ }
  # voir /etc/haproxy/maps/framaspace.map
  use_backend     %[req.hdr(host),lower,map(/etc/haproxy/maps/framaspace.map,nginx_servers)]

  # Quelques options
  option          forwardfor header X-Real-IP
  option          http-server-close
  capture         request header Host len 200

  # On surcharge le format de log pour avoir le port
  log-format      "%{+Q}o %{-Q}ci port:%cp - [%trg] %r %ST %B \"\" \"\" %cp %ms %ft %b %s %TR %Tw %Tc %Tr %Ta %tsc %ac %fc %bc %sc %rc %sq %bq %CC %CS %hrl %hsl"

backend nginx_servers
  balance      roundrobin
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
{%- for hostname, value in hosts.items() | sort -%}
{%-   if hostname is match('^nginx') -%}
{%-     set weight = weights[hostname] | default(1) %}
  server       {{ hostname | lower | replace('-', '') }} {{ value['ip'] }}:80 check weight {{ weight }}{%- endif -%}
{% endfor %}

backend minio
  balance      roundrobin
  # Gros timeout pour les update
  timeout      server 600000
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
  server       sidekick 127.0.0.1:8042 check

backend minio-rep
  balance      roundrobin
  # Gros timeout pour les update
  timeout      server 600000
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
  server       sidekick-rep 127.0.0.1:8043 check

backend minio-admin
  balance      roundrobin
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
{%- for hostname, value in hosts.items() | sort -%}
{%-   if hostname is match('^minio-[0-9]') %}
  server       {{ hostname | lower | replace('-', '') }} {{ value['ip'] }}:44563 check{%- endif -%}
{% endfor %}

backend minio-rep-admin
  balance      roundrobin
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
{%- for hostname, value in hosts.items() | sort -%}
{%-   if hostname is match('^minio-rep') %}
  server       {{ hostname | lower | replace('-', '') }} {{ value['ip'] }}:44563 check{%- endif -%}
{% endfor %}

backend imaginary_servers
  balance      roundrobin
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
{%- for hostname, value in hosts.items() | sort -%}
{%-   if hostname is match('^imaginary') %}
  server       {{ hostname | lower | replace('-', '') }} {{ value['ip'] }}:9000 check{%- endif -%}
{% endfor %}
