backend {{ office_server }}-{{ data['office_port'] }}
  http-request  set-header X-Real-Port %[src_port]
  http-request  add-header X-Forwarded-Proto https if { ssl_fc }
{% if data['office_type'] != 20 %}
  http-response add-header Cross-Origin-Resource-Policy cross-origin unless { res.hdr(Cross-Origin-Resource-Policy) -m found }
  option        http-server-close
  timeout       tunnel 1h
{% endif %}
  server        {{ office_server | lower | replace('-', '_') }} {{ hosts[office_server]['ip'] }}:{{ data['office_port'] }} check
