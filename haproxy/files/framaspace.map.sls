{% for i, data in instances.items() -%}
{%   set office_server = 'office-1' %}
{%   if 'office_server' in data %}
{%     set office_server = data['office_server'] %}
{%   endif %}
{{ i }}.office.{{ domain }} {{ office_server }}-{{ data['office_port'] }}
{% endfor %}
{% for i, data in shared_offices.items() -%}
{{ i }}.office.{{ domain }} {{ data['server'] }}-{{ data['port'] }}
{% endfor %}
