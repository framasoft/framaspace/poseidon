module(load="omrelp")
if $programname == 'haproxy' then {
{%- for hostname, value in hosts.items()|sort -%}
{%-   if hostname is match('^syslog') %}
  action(type="omrelp" target="{{ value['ip'] }}" port="2514"){%- endif -%}
{% endfor %}
}
