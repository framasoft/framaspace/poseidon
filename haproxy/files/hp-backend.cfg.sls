backend {{ hp_backend_server }}-{{ data['nc_hp_backend'] }}
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
  server       {{ hp_backend_server | lower | replace('-', '_') }} {{ hosts[hp_backend_server]['ip'] }}:{{ data['nc_hp_backend'] }} check
