backend paheko-{{ data['paheko_shared_server'] }}
  http-request set-header X-Real-Port %[src_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
  server       {{ data['paheko_shared_server'] | lower | replace('-', '') }} {{ hosts[data['paheko_shared_server']]['ip'] }}:80 check
