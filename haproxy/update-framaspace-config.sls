{% from "framaspace/map.jinja" import hosts with context %}
{% set f = pillar['framaspace'] | default({}) %}
/etc/haproxy/framaspace.cfg:
  file.managed:
    - source: salt://framaspace/haproxy/files/framaspace.cfg.sls
    - template: jinja
    - default:
      hosts: {{ hosts }}
      domain: {{ f['domain'] }}
      weights: {{ f['nginx']['weights'] | default({}) }}

reload-haproxy:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/framaspace.cfg
