{% from "framaspace/map.jinja" import hosts with context %}
{% set f              = pillar['framaspace'] | default({}) %}
{% set fi             = pillar['framaspace-instances'] | default({}) %}
{% set active         = fi['active']   | default({}) %}
{% set inactive       = fi['inactive'] | default({}) %}
{% set shared_offices = {} %}

/etc/haproxy/maps/space_disabled_because_inactive.txt:
  file.managed:
    - source: salt://framaspace/haproxy/files/space_disabled_because_inactive.txt.sls
    - template: jinja
    - default:
      domain: {{ f['domain'] }}
      instances: {{ inactive }}

{% for i, data in active.items() %}
{%   set office_server = 'office-1' %}
{%   if 'office_server' in data %}
{%     set office_server = data['office_server'] %}
{%   endif %}
office-backend-{{ i }}:
  file.managed:
    - name: /etc/haproxy/office-backends/{{ office_server }}-{{ data['office_port'] }}.cfg
    - source: salt://framaspace/haproxy/files/office-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}
      office_server: {{ office_server }}
{%   if data['office_type'] == 30 %}
{%     do shared_offices.update({'shared-%s-%s' % (office_server.replace('office-', ''), data['office_port']) : {'server': office_server, port: data['office_port']} }) %}
{%   endif %}

{%   if 'paheko_shared_server' in data %}
paheko-backend-{{ i }}:
  file.managed:
    - name: /etc/haproxy/paheko-backends/{{ data['paheko_shared_server'] }}.cfg
    - source: salt://framaspace/haproxy/files/paheko-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}
{%   endif %}

# hp-backend-{{ i }}:
#   file.managed:
#     - name: /etc/haproxy/high-performance-backends/{{ data['nc_hp_backend_server'] | default('notify-push-1') }}-{{ data['nc_hp_backend'] }}.cfg
#     - source: salt://framaspace/haproxy/files/hp-backend.cfg.sls
#     - makedirs: True
#     - template: jinja
#     - default:
#       hp_backend_server: {{ data['nc_hp_backend_server'] | default('notify-push-1') }}
#       hosts: {{ hosts }}
#       data: {{ data }}
{% endfor %}

/etc/haproxy/maps/framaspace.map:
  file.managed:
    - source: salt://framaspace/haproxy/files/framaspace.map.sls
    - template: jinja
    - makedirs: True
    - default:
      domain: {{ f['domain'] }}
      instances: {{ active }}
      shared_offices: {{ shared_offices }}

/etc/haproxy/maps/paheko.map:
  file.managed:
    - source: salt://framaspace/haproxy/files/paheko.map.sls
    - template: jinja
    - makedirs: True
    - default:
      domain: {{ f['domain'] }}
      instances: {{ active }}

#/etc/haproxy/maps/high-performance-backends.map:
#  file.managed:
#    - source: salt://framaspace/haproxy/files/hp-backend.map.sls
#    - template: jinja
#    - makedirs: True
#    - default:
#      domain: {{ f['domain'] }}
#      instances: {{ active }}

reload-haproxy-create:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/framaspace.map
      - file: /etc/haproxy/maps/paheko.map
      - file: /etc/haproxy/office-backends/*
      - file: /etc/haproxy/paheko-backends/*
  cmd.run:
    - name: 'echo reload | nc -U /run/haproxy-master.sock'
    - watch:
      - file: /etc/haproxy/maps/space_disabled_because_inactive.txt
