#haproxy_fb_module:
#  file.managed:
#    - name: /etc/filebeat/modules.d/haproxy.yml
#    - source: salt://framaspace/haproxy/files/fb_module.yml
#
#restart_filebeat:
#  service.running:
#    - name: filebeat
#    - watch:
#      - file: /etc/filebeat/modules.d/haproxy.yml

#rsyslog-relp:
#  pkg.installed:
#    - name: rsyslog-relp
#  file.managed:
#    - name: /etc/rsyslog.d/00-distant-syslog.conf
#    - source: salt://framaspace/haproxy/files/distant-syslog.conf.sls
#    - template: jinja
#    - default:
#      hosts: { { hosts } }
#  service.running:
#    - name: rsyslog
#    - reload: False
#    - watch:
#      - file: /etc/rsyslog.d/00-distant-syslog.conf
