{% from "framaspace/map.jinja" import hosts with context %}
{% set j = pillar['framaspace-jobs-office'] %}
{% for i, data in j.items() %}
{#   Shared collabora #}
{%   if data['office_type'] == 30 %}
remove-backend-{{ i }}:
  file.absent:
    - name: /etc/haproxy/office-backends/{{ data['old_office_server'] }}-{{ data['old_office_port'] }}.cfg
add-shared-office-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/framaspace.map
    - text: {{ 'shared-%s-%s' % (data['office_server'].replace('office-', ''), data['office_port']) }}.office.{{ pillar['framaspace']['domain'] }} {{ data['office_server'] }}-{{ data['office_port'] }}
{%   endif %}

{%   if data['office_type'] == 30 or data['old_office_type'] == 30 %}
remove-port-{{ i }}:
  file.line:
    - name: /etc/haproxy/maps/framaspace.map
    - content: {{ i }}.office.{{ pillar['framaspace']['domain'] }} {{ data['old_office_server'] }}-{{ data['old_office_port'] }}
    - mode: delete
add-port-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/framaspace.map
    - text: {{ i }}.office.{{ pillar['framaspace']['domain'] }} {{ data['office_server'] }}-{{ data['office_port'] }}
{%   endif %}

backend-{{ i }}:
  file.managed:
    - name: /etc/haproxy/office-backends/{{ data['office_server'] }}-{{ data['office_port'] }}.cfg
    - source: salt://framaspace/haproxy/files/office-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}
      office_server: {{ data['office_server'] }}
{% endfor %}

reload-haproxy-change-office:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/framaspace.map
      - file: /etc/haproxy/office-backends/*
