{% from "framaspace/map.jinja" import hosts with context %}
{% for i, data in pillar['framaspace-jobs-deploy-paheko'].items() %}
{# Paheko #}
create-paheko-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/paheko.map
    - text: {{ i }}.paheko.{{ pillar['framaspace']['domain'] }} paheko-{{ data['paheko_shared_server'] }}

paheko-backend-{{ i }}:
  file.managed:
    - name: /etc/haproxy/paheko-backends/{{ data['paheko_shared_server'] }}.cfg
    - source: salt://framaspace/haproxy/files/paheko-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}
{% endfor %}

reload-haproxy-create:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/paheko.map
      - file: /etc/haproxy/paheko-backends/*
