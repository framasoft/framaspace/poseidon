{% set j = pillar['framaspace-jobs-delete-shared-office'] %}
{% for job_id, data in j.items() %}
remove-backend-{{ data['shared_office_server'] }}-{{ data['shared_office_port'] }}:
  file.absent:
    - name: /etc/haproxy/office-backends/{{ data['shared_office_server'] }}-{{ data['shared_office_port'] }}.cfg
{% endfor %}

reload-haproxy-delete:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/office-backends/*
