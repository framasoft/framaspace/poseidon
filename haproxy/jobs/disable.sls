{% set paheko = False %}
{% for i, data in pillar['framaspace-jobs-disable'].items() %}
{# Office #}
disable-{{ i }}:
  file.line:
    - name: /etc/haproxy/maps/framaspace.map
    - content: {{ i }}.office.{{ pillar['framaspace']['domain'] }} {{ data['office_server'] }}-{{ data['office_port'] }}
    - mode: delete


{%   if data['office_type'] != 30 %}
{# Shared Collabora is office_type 30 and is deleted with a special job #}
remove-backend-{{ i }}:
  file.absent:
    - name: /etc/haproxy/office-backends/{{ data['office_server'] }}-{{ data['office_port'] }}.cfg
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/office-backends/{{ data['office_server'] }}-{{ data['office_port'] }}.cfg
{%   endif %}

{%   if 'paheko_shared_server' in data -%}
{%     set paheko = True %}
{# Paheko #}
{# The backend will be deleted with a special job, to be implemented #}
disable-paheko-{{ i }}:
  file.line:
    - name: /etc/haproxy/maps/paheko.map
    - content: {{ i }}.paheko.{{ pillar['framaspace']['domain'] }} paheko-{{ data['paheko_shared_server'] }}
    - mode: delete
{%   endif %}

{%   if 'disabling_reason' in data and data['disabling_reason'] == 20 %}
redirect-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/space_disabled_because_inactive.txt
    - text: {{ i }}.{{ pillar['framaspace']['domain'] }}
  cmd.run:
    - name: 'echo reload | nc -U /run/haproxy-master.sock'
{%   endif %}
{% endfor %}

reload-haproxy-disable:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/framaspace.map
{% if paheko %}
      - file: /etc/haproxy/maps/paheko.map
{% endif %}
