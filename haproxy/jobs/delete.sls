{% for i, data in pillar['framaspace-jobs-delete'].items() %}
{# Office #}
delete-{{ i }}:
  file.line:
    - name: /etc/haproxy/maps/framaspace.map
    - content: {{ i }}.office.{{ pillar['framaspace']['domain'] }} {{ data['office_server'] }}-{{ data['office_port'] }}
    - mode: delete

{%   if data['office_type'] != 30 %}
{# Shared Collabora is office_type 30 and is deleted with a special job #}
remove-backend-{{ i }}:
  file.absent:
    - name: /etc/haproxy/office-backends/{{ data['office_server'] }}-{{ data['office_port'] }}.cfg
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/office-backends/{{ data['office_server'] }}-{{ data['office_port'] }}.cfg
{%   endif %}

{# Paheko #}
{# The map has already been deleted in disable job #}
{# The backend will be deleted with a special job, to be implemented #}

{%   if 'disabling_reason' in data and data['disabling_reason'] == 20 %}
remove-redirect-{{ i }}:
  file.line:
    - name: /etc/haproxy/maps/space_disabled_because_inactive.txt
    - content: {{ i }}.{{ pillar['framaspace']['domain'] }}
    - mode: delete
  cmd.run:
    - name: 'echo reload | nc -U /run/haproxy-master.sock'
{%   endif %}

# delete-hpb-{{ i }}:
#   file.line:
#     - name: /etc/haproxy/maps/high-performance-backends.map
#     - content: {{ i }}.{{ pillar['framaspace']['domain'] }} {{ data['nc_hp_backend_server'] | default('notify-push-1') }}-{{ data['nc_hp_backend'] }}
#     - mode: delete
#
# remove-hp-backend-{{ i }}:
#   file.absent:
#     - name: /etc/haproxy/high-performance-backends/{{ data['nc_hp_backend_server'] | default('notify-push-1') }}-{{ data['nc_hp_backend'] }}.cfg
#   service.running:
#     - name: haproxy
#     - reload: True
#     - watch:
#       - file: /etc/haproxy/high-performance-backends/{{ data['nc_hp_backend_server'] | default('notify-push-1') }}-{{ data['nc_hp_backend'] }}.cfg
{% endfor %}

reload-haproxy-delete:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/framaspace.map
      # - file: /etc/haproxy/maps/high-performance-backends.map
