{% from "framaspace/map.jinja" import hosts with context %}
{% set paheko = False %}
{% for i, data in pillar['framaspace-jobs-create'].items() %}
{# Office #}
create-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/framaspace.map
    - text: {{ i }}.office.{{ pillar['framaspace']['domain'] }} {{ data['office_server'] }}-{{ data['office_port'] }}

{%   if data['office_type'] == 30 %}
add-shared-office-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/framaspace.map
    - text: {{ 'shared-%s-%s' % (data['office_server'].replace('office-', ''), data['office_port']) }}.office.{{ pillar['framaspace']['domain'] }} {{ data['office_server'] }}-{{ data['office_port'] }}
{%   endif %}

/etc/haproxy/office-backends/{{ data['office_server'] }}-{{ data['office_port'] }}.cfg:
  file.managed:
    - source: salt://framaspace/haproxy/files/office-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}
      office_server: {{ data['office_server'] }}

{%   if 'paheko_shared_server' in data -%}
{%     set paheko = True %}
{# Paheko #}
create-paheko-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/paheko.map
    - text: {{ i }}.paheko.{{ pillar['framaspace']['domain'] }} paheko-{{ data['paheko_shared_server'] }}

paheko-backend-{{ i }}:
  file.managed:
    - name: /etc/haproxy/paheko-backends/{{ data['paheko_shared_server'] }}.cfg
    - source: salt://framaspace/haproxy/files/paheko-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}
{%   endif %}

# create-hpb-{{ i }}:
#   file.append:
#     - name: /etc/haproxy/maps/high-performance-backends.map
#     - text: {{ i }}.{{ pillar['framaspace']['domain'] }} {{ data['nc_hp_backend_server'] | default('notify-push-1') }}-{{ data['nc_hp_backend'] }}
#
# hp-backend-{{ i }}:
#   file.managed:
#     - name: /etc/haproxy/high-performance-backends/{{ data['nc_hp_backend_server'] | default('notify-push-1') }}-{{ data['nc_hp_backend'] }}.cfg
#     - source: salt://framaspace/haproxy/files/hp-backend.cfg.sls
#     - makedirs: True
#     - template: jinja
#     - default:
#       hp_backend_server: {{ data['nc_hp_backend_server'] | default('notify-push-1') }}
#       hosts: {{ hosts }}
#       data: {{ data }}
{% endfor %}

reload-haproxy-create:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/framaspace.map
      - file: /etc/haproxy/office-backends/*
{% if paheko %}
      - file: /etc/haproxy/maps/paheko.map
      - file: /etc/haproxy/paheko-backends/*
{% endif %}
      # - file: /etc/haproxy/maps/high-performance-backends.map
      # - file: /etc/haproxy/high-performance-backends/*
