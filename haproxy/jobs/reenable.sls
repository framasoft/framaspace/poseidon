{% from "framaspace/map.jinja" import hosts with context %}
{% set paheko = False %}
{% for i, data in pillar['framaspace-jobs-reenable'].items() %}
# Office
reenable-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/framaspace.map
    - text: {{ i }}.office.{{ pillar['framaspace']['domain'] }} {{ data['office_server'] }}-{{ data['office_port'] }}

/etc/haproxy/office-backends/{{ data['office_server'] }}-{{ data['office_port'] }}.cfg:
  file.managed:
    - source: salt://framaspace/haproxy/files/office-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}

{%   if 'paheko_shared_server' in data -%}
{%     set paheko = True %}
# Paheko
reenable-paheko-{{ i }}:
  file.append:
    - name: /etc/haproxy/maps/paheko.map
    - text: {{ i }}.paheko.{{ pillar['framaspace']['domain'] }} paheko-{{ data['paheko_shared_server'] }}

paheko-backend-{{ i }}:
  file.managed:
    - name: /etc/haproxy/paheko-backends/{{ data['paheko_shared_server'] }}.cfg
    - source: salt://framaspace/haproxy/files/paheko-backend.cfg.sls
    - makedirs: True
    - template: jinja
    - default:
      hosts: {{ hosts }}
      data: {{ data }}
{%   endif %}

# create-hpb-{{ i }}:
#   file.append:
#     - name: /etc/haproxy/maps/high-performance-backends.map
#     - text: {{ i }}.{{ pillar['framaspace']['domain'] }} {{ data['nc_hp_backend_server'] | default('notify-push-1') }}-{{ data['nc_hp_backend'] }}
{%   if 'disabling_reason' in data and data['disabling_reason'] == 20 %}
remove-redirect-{{ i }}:
  file.line:
    - name: /etc/haproxy/maps/space_disabled_because_inactive.txt
    - content: {{ i }}.{{ pillar['framaspace']['domain'] }}
    - mode: delete
  cmd.run:
    - name: 'echo reload | nc -U /run/haproxy-master.sock'
{%   endif %}
{% endfor %}

reload-haproxy-reenable:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/framaspace.map
      - file: /etc/haproxy/office-backends/*
{% if paheko %}
      - file: /etc/haproxy/maps/paheko.map
      - file: /etc/haproxy/paheko-backends/*
{% endif %}
