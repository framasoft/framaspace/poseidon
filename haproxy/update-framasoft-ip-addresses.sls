{% from "framaspace/haproxy/map_ip.jinja" import framasoft_ip with context %}
/etc/haproxy/maps/ips_framasoft.txt:
  file.managed:
    - contents:
      - 10.0.20.0/24
{% for ip in framasoft_ip.keys() %}
      - {{ ip }}
{% endfor %}
reload-haproxy:
  cmd.run:
    - name: 'echo reload | nc -U /run/haproxy-master.sock'
    - watch:
      - file: /etc/haproxy/maps/ips_framasoft.txt
