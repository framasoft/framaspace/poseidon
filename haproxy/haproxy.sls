{% from "framaspace/map.jinja" import hosts with context %}
{% from "framaspace/haproxy/map_ip.jinja" import framasoft_ip with context %}
{% set f        = pillar['framaspace'] | default({}) %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
haproxy:
  pkg.installed:
    - pkgs:
      - haproxy
      - haproxyctl
      - netcat-openbsd
acme-setup:
  git.latest:
    - name: https://framagit.org/framasoft/lets-encrypt/wildcard-certs-renewal-scripts.git
    - target: /opt/wildcard-certs-renewal-scripts/
  file.managed:
    - name: /opt/wildcard-certs-renewal-scripts/gandi_api_key
    - user: root
    - group: root
    - mode: 600
    - contents: |
        {{ f['gandi-api-key'] }}
/opt/cert-renewal-haproxy.sh:
  file.managed:
    - source: salt://framaspace/haproxy/files/cert-renewal-haproxy.sh
    - mode: 755
acme-setup-space-cert:
  cmd.run:
    - name: 'certbot --email {{ pillar['root_alias'] }} --post-hook /opt/cert-renewal-haproxy.sh --agree-tos --text --renew-hook "/bin/systemctl reload haproxy" --manual --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory certonly --manual-auth-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_auth.sh --manual-cleanup-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_cleanup.sh --non-interactive --cert-name principal-domain -d {{ f['domain'] }}'
    - creates: /etc/letsencrypt/live/principal-domain/fullchain.pem
acme-setup-wilcard-space-cert:
  cmd.run:
    - name: 'certbot --email {{ pillar['root_alias'] }} --post-hook /opt/cert-renewal-haproxy.sh --agree-tos --text --renew-hook "/bin/systemctl reload haproxy" --manual --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory certonly --manual-auth-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_auth.sh --manual-cleanup-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_cleanup.sh --non-interactive --cert-name principal-domain-wildcard -d *.{{ f['domain'] }}'
    - creates: /etc/letsencrypt/live/principal-domain-wildcard/fullchain.pem
acme-setup-office-space-cert:
  cmd.run:
    - name: 'certbot --email {{ pillar['root_alias'] }} --post-hook /opt/cert-renewal-haproxy.sh --agree-tos --text --renew-hook "/bin/systemctl reload haproxy" --manual --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory certonly --manual-auth-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_auth.sh --manual-cleanup-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_cleanup.sh --non-interactive --cert-name office-wildcard -d *.office.{{ f['domain'] }}'
    - creates: /etc/letsencrypt/live/office-wildcard/fullchain.pem
acme-setup-paheko-space-cert:
  cmd.run:
    - name: 'certbot --email {{ pillar['root_alias'] }} --post-hook /opt/cert-renewal-haproxy.sh --agree-tos --text --renew-hook "/bin/systemctl reload haproxy" --manual --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory certonly --manual-auth-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_auth.sh --manual-cleanup-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_cleanup.sh --non-interactive --cert-name paheko-wildcard -d *.paheko.{{ f['domain'] }}'
    - creates: /etc/letsencrypt/live/paheko-wildcard/fullchain.pem
acme-setup-alternative-domains-cert:
  cmd.run:
    - name: 'certbot --email {{ pillar['root_alias'] }} --post-hook /opt/cert-renewal-haproxy.sh --agree-tos --text --renew-hook "/bin/systemctl reload haproxy" --manual --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory certonly --manual-auth-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_auth.sh --manual-cleanup-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_cleanup.sh --non-interactive --cert-name alternative-domains -d {{ f['alternative-domains'] | sort | join (',')}}'
    - creates: /etc/letsencrypt/live/alternative-domains/fullchain.pem
acme-setup-alternative-domains-wildcard-cert:
  cmd.run:
    - name: 'certbot --email {{ pillar['root_alias'] }} --post-hook /opt/cert-renewal-haproxy.sh --agree-tos --text --renew-hook "/bin/systemctl reload haproxy" --manual --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory certonly --manual-auth-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_auth.sh --manual-cleanup-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_cleanup.sh --non-interactive --cert-name alternative-domains-wildcard -d *.{{ f['alternative-domains'] | sort | join (',*.')}}'
    - creates: /etc/letsencrypt/live/alternative-domains-wildcard/fullchain.pem

/etc/haproxy/maps/ips_framasoft.txt:
  file.managed:
    - contents:
      - 10.0.20.0/24
{% for ip in framasoft_ip.keys() %}
      - {{ ip }}
{% endfor %}
/etc/haproxy/maps/alternative_domains.txt:
  file.managed:
    - contents:
{% for d in f['alternative-domains'] %}
      - {{ d }}
{% endfor %}
/etc/haproxy/maps/point_alternative_domains.txt:
  file.managed:
    - contents:
{% for d in f['alternative-domains'] %}
      - .{{ d }}
{% endfor %}
/etc/haproxy/framaspace.cfg:
  file.managed:
    - source: salt://framaspace/haproxy/files/framaspace.cfg.sls
    - template: jinja
    - default:
      hosts: {{ hosts }}
      domain: {{ f['domain'] }}
      weights: {{ f['nginx']['weights'] | default({}) }}
/etc/haproxy/maps/framaspace.map:
  file.managed:
    - makedirs: True
/etc/haproxy/maps/paheko.map:
  file.managed:
    - makedirs: True
/etc/haproxy/maps/space_disabled_because_inactive.txt:
  file.managed
/etc/haproxy/office-backends/:
  file.directory:
    - user: root
    - group: root
/etc/haproxy/paheko-backends/:
  file.directory:
    - user: root
    - group: root

reload-haproxy:
  service.running:
    - name: haproxy
    - reload: True
    - watch:
      - file: /etc/haproxy/maps/framaspace.map
      - file: /etc/haproxy/maps/paheko.map
      - file: /etc/haproxy/framaspace.cfg
  cmd.run:
    - name: 'echo reload | nc -U /run/haproxy-master.sock'
    - watch:
      - file: /etc/haproxy/maps/space_disabled_because_inactive.txt
      - file: /etc/haproxy/maps/ips_framasoft.txt
      - file: /etc/haproxy/maps/alternative_domains.txt
      - file: /etc/haproxy/maps/point_alternative_domains.txt

haproxy-config:
  file.line:
    - name: /etc/default/haproxy
    - match: CONFIG
    - content: CONFIG="/etc/haproxy/haproxy.cfg -f /etc/haproxy/framaspace.cfg -f /etc/haproxy/office-backends -f /etc/haproxy/paheko-backends"
    - mode: replace
  service.running:
    - name: haproxy
    - reload: False
    - watch:
      - file: /etc/default/haproxy
{% if (active | length) > 0 %}
      - file: /etc/haproxy/office-backends/*
      - file: /etc/haproxy/paheko-backends/*
{% endif %}
# - content: CONFIG="/etc/haproxy/haproxy.cfg -f /etc/haproxy/framaspace.cfg -f /etc/haproxy/office-backends -f /etc/haproxy/high-performance-backends -f /etc/haproxy/paheko-backends"
