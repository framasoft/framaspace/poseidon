[Unit]
Description=Podman container-imaginary.service
Documentation=man:podman-generate-systemd(1)
Wants=network.target
After=network-online.target
Requires=wg-quick@framaspace.service
After=wg-quick@framaspace.service

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStopSec=70
ExecStartPre=/usr/bin/perl -e 'sleep 1 until -e "/sys/class/net/framaspace"'
ExecStartPre=/bin/rm -f %t/container-imaginary.pid %t/container-imaginary.ctr-id
ExecStart=/usr/bin/podman run --conmon-pidfile %t/container-imaginary.pid --cidfile %t/container-imaginary.ctr-id --cgroups=no-conmon --replace -d --name=imaginary -p {{ ip }}:9000:9000 nextcloud/aio-imaginary:latest -cors
ExecStop=/usr/bin/podman stop --ignore --cidfile %t/container-imaginary.ctr-id -t 10
ExecStopPost=/usr/bin/podman rm --ignore -f --cidfile %t/container-imaginary.ctr-id
PIDFile=%t/container-imaginary.pid
Type=forking

[Install]
WantedBy=multi-user.target default.target
