{% from "framaspace/map.jinja" import hosts with context %}
/etc/systemd/system/container-imaginary.service:
  file.managed:
    - source: salt://framaspace/imaginary/files/container-imaginary.service.sls
    - template: jinja
    - default:
      ip: {{ hosts[grains['id']]['ip'] }}
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/container-imaginary.service
  service.running:
    - name: container-imaginary.service
    - reload: False
    - enable: True
    - watch:
      - file: /etc/systemd/system/container-imaginary.service
