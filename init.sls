{% if grains['framaspace'] %}
include:
  - .common
{%   if grains['id'] is match('^haproxy') %}
  - .haproxy
{%   elif grains['id'] is match('^nginx') %}
  - .nginx
  - .php
  - .nextcloud
  - .paheko
{%   elif grains['id'] is match('^redis') %}
  - .redis
{%   elif grains['id'] is match('^postgresql') %}
  - .postgresql
{%   elif grains['id'] is match('^minio-rep') %}
  - .miniorep
{%   elif grains['id'] is match('^minio') %}
  - .minio
{%   elif grains['id'] is match('^imaginary') %}
  - .imaginary
{%   elif grains['id'] is match('^office') %}
  - .office
{%   elif grains['id'] is match('^syslog') %}
  - .syslog
{%   elif grains['id'] is match('^notify-push') %}
  - .notify-push
{%   endif %}
{% endif %}
