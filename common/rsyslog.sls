{% from "framaspace/map.jinja" import hosts with context %}
{% set syslog_servers = [] %}
{% for hostname, value in hosts.items()|sort %}
{%   if hostname is match('^syslog') %}
{%     do syslog_servers.append(value['ip']) %}
{%   endif %}
{% endfor %}
rsyslog-relp:
  pkg.installed:
    - name: rsyslog-relp
  file.managed:
    - name: /etc/rsyslog.d/00-distant-syslog.conf
    - source: salt://framaspace/common/files/distant-syslog.conf.sls
    - template: jinja
    - default:
      hosts: {{ hosts }}
      syslog_servers: {{ syslog_servers }}
  service.running:
    - name: rsyslog
    - reload: False
    - watch:
      - file: /etc/rsyslog.d/00-distant-syslog.conf
