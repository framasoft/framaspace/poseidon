prepare_for_sidekick:
  pkg.installed:
    - pkgs:
      - minisign
  user.present:
    - name: minio-sidekick
    - shell: /bin/false
    - home: /opt/sidekick_home
    - createhome: True
    - system: True
  alias.present:
    - name: minio-sidekick
    - target: root
  file.managed:
    - name: /opt/sidekick_home/bin/sidekick-linux-amd64.minisig
    - source: "https://github.com/minio/sidekick/releases/latest/download/sidekick-linux-amd64.minisig"
    - makedirs: True
    - skip_verify: True
    - mode: 755
    - user: minio-sidekick
    - group: minio-sidekick
    - keep_source: False
/opt/sidekick_home/bin/sidekick-linux-amd64:
  file.managed:
    - source: "https://github.com/minio/sidekick/releases/latest/download/sidekick-linux-amd64"
    - makedirs: True
    - skip_verify: True
    - mode: 755
    - user: minio-sidekick
    - group: minio-sidekick
    - keep_source: False
  cmd.run:
    - name: minisign -Vm sidekick-linux-amd64 -P RWTx5Zr1tiHQLwG9keckT0c45M3AGeHD6IvimQHpyRywVWGbP1aVSGav && mv sidekick-linux-amd64 sidekick && rm sidekick-linux-amd64.minisig
    - cwd: /opt/sidekick_home/bin/
    - runas: minio-sidekick
