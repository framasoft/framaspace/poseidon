{% from "framaspace/map.jinja" import hosts, minio with context %}
{% set volumes = [] %}
{% set volumes_rep = [] %}
{% for pool, disks in pillar['framaspace']['minio']['pools'].items() | sort %}
{%   do volumes.append('http://10.0.60.{' ~ pool  ~ '}:9000') %}
{%   do volumes_rep.append('http://10.0.100.{' ~ pool  ~ '}:9000') %}
{% endfor %}
/etc/default/sidekick:
  file.managed:
    - mode: 600
    - contents:
      - 'SIDEKICK_8042_OPTS="--quiet --health-path=/minio/health/ready --address 127.0.0.1:8042"'
      - 'SIDEKICK_8042_SERVERS="{{ volumes | join(' ') }}"'
{% if grains['id'] is match('^haproxy-') %}
      - 'SIDEKICK_8043_OPTS="--quiet --health-path=/minio/health/ready --address 127.0.0.1:8043"'
      - 'SIDEKICK_8043_SERVERS="{{ volumes_rep | join(' ') }}"'
{% endif %}
sidekick.service:
  file.managed:
    - name: /etc/systemd/system/sidekick@.service
    - source: salt://framaspace/common/sidekick/files/sidekick@.service
  service.running:
    - name: sidekick@8042.service
    - restart: True
    - enable: True
    - watch:
      - file: /etc/default/sidekick
      - file: /etc/systemd/system/sidekick@.service
{% if grains['id'] is match('^haproxy-') %}
sidekick-rep.service:
  service.running:
    - name: sidekick@8043.service
    - restart: True
    - enable: True
    - watch:
      - file: /etc/default/sidekick
      - file: /etc/systemd/system/sidekick@.service
{% endif %}
