#!/bin/bash

cd /opt/sidekick_home/bin/ || exit 1
minisign -Vm sidekick-linux-amd64 -P RWTx5Zr1tiHQLwG9keckT0c45M3AGeHD6IvimQHpyRywVWGbP1aVSGav &&
    systemctl stop sidekick@8042.service &&
{% if is_haproxy %}
    systemctl stop sidekick@8043.service &&
{% endif %}
    mv sidekick-linux-amd64 sidekick &&
    systemctl start sidekick@8042.service &&
{% if is_haproxy %}
    systemctl start sidekick@8043.service &&
{% endif %}
    systemctl status sidekick@8042.service &&
{% if is_haproxy %}
    systemctl status sidekick@8043.service &&
{% endif %}
    rm sidekick-linux-amd64.minisig
