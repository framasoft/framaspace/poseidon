{% if salt['file.file_exists']('/etc/systemd/system/sidekick@.service') %}
/opt/sidekick_home/bin/sidekick-linux-amd64.minisig:
  file.managed:
    - source: "https://github.com/minio/sidekick/releases/latest/download/sidekick-linux-amd64.minisig"
    - makedirs: True
    - skip_verify: True
    - mode: 755
    - keep_source: false
    - user: minio-sidekick
    - group: minio-sidekick
/opt/sidekick_home/bin/sidekick-linux-amd64:
  file.managed:
    - source: "https://github.com/minio/sidekick/releases/latest/download/sidekick-linux-amd64"
    - makedirs: True
    - skip_verify: True
    - mode: 755
    - keep_source: false
    - user: minio-sidekick
    - group: minio-sidekick
verify-and-replace-sidekick:
  cmd.script:
    - source: salt://framaspace/common/sidekick/files/update-sidekick.sh.sls
    - cwd: /
    - template: jinja
    - defaults:
      is_haproxy: {{ True if grains['id'] is match('^haproxy-') else False }}
{% else %}
avoid-bad-status:
  cmd.run:
    - name: 'echo "Pas de Sidekick ici"'
{% endif %}
