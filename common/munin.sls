/etc/munin/plugins/exim_mailqueue:
  file.symlink:
    - target: /usr/share/munin/plugins/exim_mailqueue
/etc/munin/plugins/exim_mailstats:
  file.symlink:
    - target: /usr/share/munin/plugins/exim_mailstats
/etc/munin/plugins/fail2ban:
  file.symlink:
    - target: /usr/share/munin/plugins/fail2ban
/etc/munin/plugins/if_wg0:
  file.symlink:
    - target: /usr/share/munin/plugins/if_
/etc/munin/plugins/if_err_wg0:
  file.symlink:
    - target: /usr/share/munin/plugins/if_err_

{% if grains['id'] is match('^postgresql') %}
/etc/munin/plugins/postgres_autovacuum:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_autovacuum
/etc/munin/plugins/postgres_bgwriter:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_bgwriter
/etc/munin/plugins/postgres_cache_:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_cache_ALL
/etc/munin/plugins/postgres_connections_:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_connections_ALL
/etc/munin/plugins/postgres_locks_:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_locks_ALL
/etc/munin/plugins/postgres_querylength_:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_querylength_ALL
/etc/munin/plugins/postgres_size_:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_size_ALL
/etc/munin/plugins/postgres_transactions_:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_transactions_ALL
/etc/munin/plugins/postgres_users:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_users
/etc/munin/plugins/postgres_xlog:
  file.symlink:
    - target: /usr/share/munin/plugins/postgres_xlog
{% elif grains['id'] is match('^minio') %}
/etc/munin/plugins/hddtemp_smartctl:
  file.symlink:
    - target: /usr/share/munin/plugins/hddtemp_smartctl
{% endif %}

framaspace-munin-node:
  service.running:
    - name: munin-node
    - reload: False
    - watch:
      - file: /etc/munin/plugins/*
