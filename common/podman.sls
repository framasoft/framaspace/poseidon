podman:
  pkg.installed:
    - pkgs:
      - podman
      - crun
  file.symlink:
    - name: /usr/bin/docker
    - target: /usr/bin/podman
/etc/containers/registries.conf:
  ini.options_present:
    - separator: '='
    - sections:
        unqualified-search-registries: '["docker.io"]'
/opt/clean-old-containers.sh:
  file.managed:
    - source: salt://framaspace/common/files/clean-old-containers.sh
    - mode: 754
    - user: root
    - group: root
