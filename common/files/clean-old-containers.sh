#!/bin/bash

export OUTPUT=/dev/stdout

while getopts ":q" option; do
    case ${option} in
        q)
            export OUTPUT=/dev/null
            ;;
        *)  ;;
    esac
done

BEFORE=$(df -h /var)
RMI=$(podman images -qa -f dangling=true)
if [ -n "$RMI" ]; then
    # shellcheck disable=SC2086
    podman rmi $RMI >"$OUTPUT"
fi
RMI=$(podman volume ls -qf dangling=true)
if [ -n "$RMI" ]; then
    # shellcheck disable=SC2086
    podman volume rm $RMI >"$OUTPUT"
fi
podman system prune -a -f

AFTER=$(df -h /var)
cat <<EOF >"$OUTPUT"
------
Before
------
$BEFORE

-----
After
-----
$AFTER
EOF
