module(load="omrelp")

template(name="LongTagForwardFormat" type="string"
    string="<%PRI%>%TIMESTAMP:::date-rfc3339% %HOSTNAME% %syslogtag:1:64%%msg:::sp-if-no-1st-sp%%msg%"
)

{% for program in ['haproxy', 'nginx', 'redis', 'postgres', 'minio', 'notify-push'] -%}
{%-   if grains['id'] is match('^%s' % program) %}
# {{ program | capitalize }}
if $programname == '{{ program }}' then {
{%-     for ip in syslog_servers %}
  action(type="omrelp" target="{{ ip }}" port="2514" template="LongTagForwardFormat"){% endfor %}
{%-     if program != 'haproxy' %}
  stop{%-     endif %}
}
{%   endif -%}
{% endfor -%}

{% if grains['id'] is match('^(haproxy|nginx)-') -%}
# Sidekick
if $programname startswith 'minio-sidekick-' then {
{%-  for ip in syslog_servers %}
  action(type="omrelp" target="{{ ip }}" port="2514" template="LongTagForwardFormat"){% endfor %}
  stop
}
{% endif -%}

{% if grains['id'] is match('^nginx-') -%}
# Pgpool
if $programname == 'pgpool' then {
{%-  for ip in syslog_servers %}
  action(type="omrelp" target="{{ ip }}" port="2514" template="LongTagForwardFormat"){% endfor %}
  stop
}
# Nextcloud
if $programname contains('nc-framaspace') then {
{%-  for ip in syslog_servers %}
  action(type="omrelp" target="{{ ip }}" port="2514" template="LongTagForwardFormat"){% endfor %}
  stop
}
# Chronos
if $programname contains('chronos') then {
{%-  for ip in syslog_servers %}
  action(type="omrelp" target="{{ ip }}" port="2514" template="LongTagForwardFormat"){% endfor %}
  stop
}
{% endif -%}
