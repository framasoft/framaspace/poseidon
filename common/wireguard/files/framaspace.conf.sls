[Interface]
Address = {{ hosts[id]['ip'] }}/8
SaveConfig = false
ListenPort = 42524
PrivateKey = {{ hosts[id]['wg']['priv'] }}

{%- for hostname, value in hosts.items() | sort -%}
{%-   if hostname != id -%}
{%-      if hostname is match(regex) %}

# {{ hostname | capitalize }}
[Peer]
PublicKey = {{ value['wg']['pub'] }}
Endpoint = {{ hostname }}.framasoft.org:42524
AllowedIPs = {{ value['ip'] }}/32
PersistentKeepalive = 21{%-     endif -%}
{%-   endif -%}
{% endfor %}

{% if 'wg' in extra %}
{%   for hostname in extra['wg'] | sort -%}
{%-     if hostname in pillar['hosts'] and pillar['hosts'][hostname]['wg'] is defined and hostname in extra['servers'] %}
# {{ hostname | capitalize }}
[Peer]
PublicKey = {{ pillar['hosts'][hostname]['wg']['pub'] }}
Endpoint = {{ hostname }}.framasoft.org:42524
AllowedIPs = {{ extra['servers'][hostname] }}/32
PersistentKeepalive = 21{%-     endif -%}
{%   endfor -%}
{% endif -%}
