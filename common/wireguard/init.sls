{% from "framaspace/map.jinja" import hosts with context %}
{% set f     = pillar['framaspace'] %}
{% set id    = grains['id'] %}
{% set reg   = [] %}
{% set extra = {'servers': f['wg_extra']['servers']} %}
{% for hostname, value in hosts.items()|sort %}
{%   if id is match('^haproxy') %}
{#     if hostname is match('^(minio|nginx|office|syslog|imaginary|notify-push)') #}
{%     if hostname is match('^(minio|nginx|office|syslog|imaginary)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%   elif id is match('^nginx') %}
{%     if hostname is match('^(haproxy|minio|imaginary|postgresql|redis|syslog)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%   elif id is match('^redis') %}
{#     if hostname is match('^(nginx|redis|syslog|notify-push)') #}
{%     if hostname is match('^(nginx|redis|syslog)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%   elif id is match('^postgresql') %}
{#     if hostname is match('^(nginx|postgresql|syslog|notify-push)') #}
{%     if hostname is match('^(nginx|postgresql|syslog)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%   elif id is match('^minio') %}
{%     if hostname is match('^(minio|syslog|haproxy|nginx)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%     do extra.update({'wg': f['wg_extra']['settings']['minio']}) %}
{%   elif id is match('^imaginary') %}
{%     if hostname is match('^(haproxy|syslog)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%   elif id is match('^office') %}
{%     if hostname is match('^(haproxy|syslog)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%   elif id is match('^syslog') %}
{%       do reg.append(hostname) %}
{%   elif id is match('^notify-push') %}
{%     if hostname is match('^(haproxy|postgresql|redis|syslog)') %}
{%       do reg.append(hostname) %}
{%     endif %}
{%   endif %}
{% endfor %}
wireguard:
  pkg.installed
/etc/wireguard/framaspace.conf:
  file.managed:
    - source: salt://framaspace/common/wireguard/files/framaspace.conf.sls
    - template: jinja
    - user: root
    - group: root
    - mode: 600
    - default:
      hosts: {{ hosts }}
      id: {{ id }}
      regex: '^({{ reg | join('|') }})$'
      extra: {{ extra }}
  service.running:
    - name: wg-quick@framaspace.service
    - enable: True
    - reload: True
    - watch:
      - file: /etc/wireguard/framaspace.conf
