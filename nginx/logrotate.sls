/etc/logrotate.d/nginx:
  pkg.installed:
    - name: pbzip2
  file.managed:
    - source: salt://framaspace/nginx/files/logrotate.nginx
