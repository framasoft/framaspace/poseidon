server {
    listen {{ hosts[grains['id']]['ip'] }}:80;
    server_name ~.*\.paheko\.{{ domain }};
{% for hostname, value in hosts.items()|sort -%}
{%-   if hostname is match('^haproxy') %}
    set_real_ip_from {{ value['ip'] }};{%- endif -%}
{% endfor %}

    access_log syslog:server=unix:/run/systemd/journal/syslog,nohostname combined_with_real_port;
    access_log /var/log/nginx/paheko/$host.access.log combined_with_real_port;
    error_log syslog:server=unix:/run/systemd/journal/syslog,nohostname;
    error_log /var/log/nginx/paheko.{{ domain }}.error.log;

    # Path to the root of your installation
    root /var/www/paheko/production/src/www;

    # Access to error pages
    location ~ ^/(?:404|500)\.html$ {
        root /opt/demeter/status_codes/;
        internal;
    }
    location ~ ^/(?:404|500)\.jpg$ {
        root /opt/demeter/status_codes/;
    }

    # Prevent nginx HTTP Server Detection
    server_tokens off;

    # HSTS settings
    # WARNING: Only add the preload option once you read about
    # the consequences in https://hstspreload.org/. This option
    # will add the domain to a hardcoded list that is shipped
    # in all major browsers and getting removed from this list
    # could take several months.
    add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;" always;

    add_header Cross-Origin-Resource-Policy cross-origin;
    add_header Cross-Origin-Embedder-Policy require-corp;

    # set max upload size and increase upload timeout:
    client_max_body_size 10M;

    location ~ ^/\.(cache|well-known/assetlinks.json)  {
        return 404;
    }
    location ~ ^/include/|/templates/|^/scripts/|^/bin/|/data/|/.*\.log|/(README|VERSION|COPYING|Makefile|pubkey.asc)|/config\.(.*)\.php|/sous-domaine\.html|_inc\.php {
        return 403;
    }

    # https://fossil.kd2.org/paheko/file?name=src/apache-vhost.conf&ci=tip
    # https://github.com/openresty/lua-nginx-module#rewrite_by_lua_block
    location / {
        try_files $uri $uri/ /_route.php?$query_string;
        index index.php /_route.php;
    }

    location ~ \.php {
        try_files $uri $uri/ /_route.php?$query_string;
        include fastcgi.conf;
        fastcgi_param HTTPS 'on';
        fastcgi_pass  127.0.0.1:9000;
    }
}
