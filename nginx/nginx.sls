{% from "framaspace/map.jinja" import hosts with context %}
{% set domain = pillar['framaspace']['domain'] %}
nginx:
  pkg.installed

nginx-dependency:
  file.managed:
    - name: /etc/systemd/system/nginx.service.d/override.conf
    - makedirs: True
    - contents:
      - '[Unit]'
      - Requires=wg-quick@framaspace.service
      - After=wg-quick@framaspace.service
      - '[Service]'
      - ExecStartPre=/usr/bin/perl -e 'sleep 1 until -e "/sys/class/net/framaspace"'
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/nginx.service.d/override.conf

/etc/nginx/conf.d/log_format_combined_with_real_port.conf:
  file.managed:
    - source: salt://framaspace/nginx/files/log_format_combined_with_real_port.conf

/etc/nginx/conf.d/map-x-forwarded-proto.conf:
  file.managed:
    - source: salt://framaspace/nginx/files/map-x-forwarded-proto.conf

/etc/nginx/sites-available/zzz.{{ domain }}:
  file.managed:
    - source: salt://framaspace/nginx/files/zzz.{{ domain }}.sls
    - template: jinja
    - default:
      domain: {{ domain }}
      hosts: {{ hosts }}
/etc/nginx/sites-enabled/zzz.{{ domain }}:
  file.symlink:
    - target: ../sites-available/zzz.{{ domain }}

/etc/nginx/sites-available/paheko.{{ domain }}:
  file.managed:
    - source: salt://framaspace/nginx/files/paheko.{{ domain }}.sls
    - template: jinja
    - default:
      domain: {{ domain }}
      hosts: {{ hosts }}
/etc/nginx/sites-enabled/paheko.{{ domain }}:
  file.symlink:
    - target: ../sites-available/paheko.{{ domain }}

/var/log/nginx/nextcloud/:
  file.directory:
    - user: www-data
    - group: adm
    - mode: 755

/var/log/nginx/paheko/:
  file.directory:
    - user: www-data
    - group: adm
    - mode: 755

reload-framaspace-nginx:
  service.running:
    - name: nginx
    - reload: True
    - watch:
      - file: /etc/nginx/conf.d/log_format_combined_with_real_port.conf
      - file: /etc/nginx/conf.d/map-x-forwarded-proto.conf
      - file: /etc/nginx/sites-available/zzz.{{ domain }}
      - file: /etc/nginx/sites-available/paheko.{{ domain }}
      - file: /var/log/nginx/nextcloud/
      - file: /var/log/nginx/paheko/
