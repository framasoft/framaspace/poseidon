include:
  - framaspace.common.rsyslog
  - framaspace.common.sidekick
  - .nginx
  - .logrotate
