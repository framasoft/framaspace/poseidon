{% from "framaspace/map.jinja" import hosts with context %}
{% set redis_servers = [] %}
{% for hostname, value in hosts.items()|sort %}
{%   if hostname is match('^redis') %}
{%     do redis_servers.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}

redis:
  pkg.installed

dependency:
  file.managed:
    - name: /etc/systemd/system/redis-server.service.d/override.conf
    - makedirs: True
    - contents:
      - '[Unit]'
      - Requires=wg-quick@framaspace.service
      - After=wg-quick@framaspace.service
      - Requires=sys-devices-virtual-net-framaspace.device
      - After=sys-devices-virtual-net-framaspace.device
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/redis-server.service.d/override.conf

listen:
  file.replace:
    - name: /etc/redis/redis.conf
    - pattern: '^bind 127.0.0.1 ::1'
    - repl: 'bind {{ hosts[grains['id']]['ip'] }}'
protected:
  file.replace:
    - name: /etc/redis/redis.conf
    - pattern: '^protected-mode yes'
    - repl: 'protected-mode no'
cluster-enabled:
  file.replace:
    - name: /etc/redis/redis.conf
    - pattern: '# cluster-enabled .*'
    - repl: 'cluster-enabled yes'
cluster-require-full-coverage:
  file.replace:
    - name: /etc/redis/redis.conf
    - pattern: '.?.?cluster-require-full-coverage yes'
    - repl: 'cluster-require-full-coverage no'
logging-to-syslog:
  file.replace:
    - name: /etc/redis/redis.conf
    - pattern: '.?.?syslog-enabled no'
    - repl: 'syslog-enabled yes'
no-logo:
  file.replace:
    - name: /etc/redis/redis.conf
    - pattern: 'always-show-logo yes'
    - repl: 'always-show-logo no'
databases:
  file.replace:
    - name: /etc/redis/redis.conf
    - pattern: 'databases 16'
    - repl: 'databases 3'

vm.overcommit_memory:
  sysctl.present:
    - value: 1

enable-transparent_hugepage:
  cmd.run:
    - name: "if [[ $(grep -vF '[madvise]' /sys/kernel/mm/transparent_hugepage/enabled) ]]; then echo madvise > /sys/kernel/mm/transparent_hugepage/enabled; fi"
    - shell: /bin/bash
  file.managed:
    - name: /etc/rc.local
    - mode: 755
    - contents:
      - '#!/bin/bash'
      - echo madvise > /sys/kernel/mm/transparent_hugepage/enabled
      - exit 0
  service.running:
    - name: rc-local
    - enable: True

redis-server.service:
  service.running:
    - reload: False
    - watch:
      - file: /etc/redis/redis.conf
      - sysctl: vm.overcommit_memory

redis-cluster-creation:
  cmd.run:
    - name: "redis-cli --cluster create {{ redis_servers | join(' ') }} --cluster-yes --cluster-replicas 1"
    - creates: /var/lib/redis/nodes.conf
