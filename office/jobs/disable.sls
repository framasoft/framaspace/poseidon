{% set j = pillar['framaspace-jobs-disable'] %}
{% for i, data in j.items() %}
{%   if data['office_server'] == grains['id'] %}

default-collabora-{{ i }}:
  file.line:
    - name: /etc/default/collabora
    - content: "DOMAIN_{{ data['office_port'] }}='{{ i }}'"
    - mode: delete
default-onlyoffice-{{ i }}:
  file.line:
    - name: /etc/default/onlyoffice
    - content: "SECRET_{{ data['office_port'] }}='{{ data['oo_secret'] }}'"
    - mode: delete

{%     if data['office_type'] == 10 %}
# Collabora
container-collabora@{{ data['office_port'] }}.service:
  service.dead:
    - enable: False
{%     elif data['office_type'] == 20 %}
# Onlyoffice
container-onlyoffice@{{ data['office_port'] }}.service:
  service.dead:
    - enable: False
{#%     elif data['office_type'] == 30 %#}
{# Shared office container are stopped with a special job #}
{%     endif %}

{%   else %}
nothing-to-do-here-{{ i }}:
  cmd.run:
    - name: echo "This server is not concerned by the job."
{%   endif %}
{% endfor %}
