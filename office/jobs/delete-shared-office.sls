{% set j = pillar['framaspace-jobs-delete-shared-office'] %}
{% for job_id, data in j.items() %}
{%   if data['shared_office_server'] == grains['id'] %}
container-shared-collabora@{{ data['shared_office_port'] }}.service:
  service.dead:
    - enable: False

{%   else %}
nothing-to-do-here-{{ job_id }}:
  cmd.run:
    - name: echo "This server is not concerned by the job."
{%   endif %}
{% endfor %}
