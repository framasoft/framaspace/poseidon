{% set j = pillar['framaspace-jobs-create'] %}
{% for i, data in j.items() %}
{%   if data['office_server'] == grains['id'] %}

default-collabora-{{ i }}:
  file.append:
    - name: /etc/default/collabora
    - text: "DOMAIN_{{ data['office_port'] }}='{{ i }}'"
default-onlyoffice-{{ i }}:
  file.append:
    - name: /etc/default/onlyoffice
    - text: "SECRET_{{ data['office_port'] }}='{{ data['oo_secret'] }}'"

{%     if data['office_type'] == 10 %}
# Collabora
container-collabora@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     elif data['office_type'] == 20 %}
# Onlyoffice
container-onlyoffice@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     elif data['office_type'] == 30 %}
# Shared collabora
container-shared-collabora@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     endif %}

{%   else %}
nothing-to-do-here-{{ i }}:
  cmd.run:
    - name: echo "This server is not concerned by the job."
{%   endif %}
{% endfor %}
