{% set j = pillar['framaspace-jobs-office'] %}
{% for i, data in j.items() %}
{%   if data['old_office_server'] != grains['id'] and data['office_server'] != grains['id'] %}
nothing-to-do-here-{{ i }}:
  cmd.run:
    - name: echo "This server is not concerned by the job."
{%   endif %}
{%   if data['old_office_server'] == grains['id'] %}

# Deactivate old office backend
{%     if data['old_office_type'] == 10 %}
# Collabora
container-collabora@{{ data['old_office_port'] }}.service:
  service.dead:
    - enable: False
{%     elif data['old_office_type'] == 20 %}
# Onlyoffice
container-onlyoffice@{{ data['old_office_port'] }}.service:
  service.dead:
    - enable: False
{#%     elif data['old_office_type'] == 30 %#}
{# Shared office container are stopped with a special job #}
{%     endif %}

# Remove old office config
rm-default-collabora-{{ i }}:
  file.line:
    - name: /etc/default/collabora
    - content: "DOMAIN_{{ data['old_office_port'] }}='{{ i }}'"
    - mode: delete
rm-default-onlyoffice-{{ i }}:
  file.line:
    - name: /etc/default/onlyoffice
    - content: "SECRET_{{ data['old_office_port'] }}='{{ data['oo_secret'] }}'"
    - mode: delete
{%   endif %}

{%   if data['office_server'] == grains['id'] %}

# Add new office config
add-default-collabora-{{ i }}:
  file.append:
    - name: /etc/default/collabora
    - text: "DOMAIN_{{ data['office_port'] }}='{{ i }}'"
add-default-onlyoffice-{{ i }}:
  file.append:
    - name: /etc/default/onlyoffice
    - text: "SECRET_{{ data['office_port'] }}='{{ data['oo_secret'] }}'"

# Activate new office backend
{%     if data['office_type'] == 10 %}
# Collabora
container-collabora@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     elif data['office_type'] == 20 %}
# Onlyoffice
container-onlyoffice@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     elif data['office_type'] == 30 %}
# Shared collabora
container-shared-collabora@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     endif %}

{%   endif %}
{% endfor %}
