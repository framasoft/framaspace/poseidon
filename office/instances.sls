include:
  - .defaults

{% set fi     = pillar['framaspace-instances'] | default({}) %}
{% set active = fi['active'] | default({}) %}
{% for i, data in active.items() %}
{%   set office_server = 'office-1' %}
{%   if 'office_server' in data %}
{%     set office_server = data['office_server'] %}
{%   endif %}
{%   if office_server == grains['id'] %}
{%     if data['office_type'] == 10 %}
# Collabora
container-collabora@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     elif data['office_type'] == 20 %}
# Onlyoffice
container-onlyoffice@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     elif data['office_type'] == 30 %}
# Shared collabora
container-shared-collabora@{{ data['office_port'] }}.service:
  service.running:
    - reload: False
    - enable: True
{%     endif %}

{%   endif %}
{% endfor %}
