{% from "framaspace/map.jinja" import hosts with context %}
/etc/systemd/system/container-onlyoffice@.service:
  file.managed:
    - source: salt://framaspace/office/files/container-onlyoffice@.service.sls
    - template: jinja
    - default:
      ip: {{ hosts[grains['id']]['ip'] }}
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/container-onlyoffice@.service
/opt/update-and-restart-onlyoffice-containers.sh:
  file.managed:
    - source: salt://framaspace/office/files/update-and-restart-onlyoffice-containers.sh
    - mode: 754
