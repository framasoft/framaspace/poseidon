{% set fi       = pillar['framaspace-instances'] %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% do active.update(inactive) %}
/etc/default/collabora:
  file.managed:
    - source: salt://framaspace/office/files/default-collabora.sls
    - template: jinja
    - default:
      f: {{ active }}
/etc/default/onlyoffice:
  file.managed:
    - source: salt://framaspace/office/files/default-onlyoffice.sls
    - template: jinja
    - default:
      f: {{ active }}
