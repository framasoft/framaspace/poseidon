{% from "framaspace/map.jinja" import hosts with context %}
/etc/systemd/system/container-shared-collabora@.service:
  file.managed:
    - source: salt://framaspace/office/files/container-shared-collabora@.service.sls
    - template: jinja
    - default:
      ip: {{ hosts[grains['id']]['ip'] }}
      fspace_domain: {{ pillar['framaspace']['domain'] }}
      escaped_fspace_domain: {{ pillar['framaspace']['domain'] | replace('.', '\\\\.') }}
      escaped_fspace_domain_regex: {{ pillar['framaspace']['domain'] | replace('.', '\\\\\\\\.') }}
      server_nb: {{ grains['id'] | replace('office-', '') }}
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/container-shared-collabora@.service
/opt/update-and-restart-shared-collabora.sh:
  file.managed:
    - source: salt://framaspace/office/files/update-and-restart-shared-collabora.sh
    - mode: 754
