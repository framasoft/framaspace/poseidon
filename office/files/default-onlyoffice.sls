{% for i, data in f.items() | sort %}
{%   set office_server = 'office-1' %}
{%   if 'office_server' in data %}
{%     set office_server = data['office_server'] %}
{%   endif %}
{%   if office_server == grains['id'] %}
SECRET_{{ data['office_port'] }}={{ data['oo_secret'] }}
{%   endif %}
{% endfor %}
