#!/bin/bash

WG_IP=$(ip -j a show dev framaspace | jq .[].addr_info[0].local -r)

systemctl daemon-reload

podman pull onlyoffice/documentserver || exit 1
cd /etc/systemd/system/multi-user.target.wants/  || exit 1
for i in container-onlyoffice@*; do
    systemctl restart "$i"
    sleep 120

    /opt/clean-old-containers.sh

    # shellcheck disable=SC2001
    nb=$(echo "$i" | sed -e "s/container-onlyoffice@\(.*\)\.service/\1/")
    while [[ $(curl -s "http://$WG_IP:$nb/healthcheck") != 'true' ]]; do
        systemctl restart "$i"
        sleep 180
    done
done

/opt/clean-old-containers.sh
