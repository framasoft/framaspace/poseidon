#!/bin/bash

systemctl daemon-reload

podman pull framasoft/collabora-online-unleashed || exit 1
cd /etc/systemd/system/multi-user.target.wants/  || exit 1
for i in container-shared-collabora@*; do
    systemctl restart "$i"
    sleep 120
done

/opt/clean-old-containers.sh
