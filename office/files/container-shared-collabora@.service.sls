[Unit]
Description=Podman container-shared-collabora-%i.service
Documentation=man:podman-generate-systemd(1)
Wants=network.target
After=network-online.target
Requires=wg-quick@framaspace.service
After=wg-quick@framaspace.service

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
Restart=on-failure
TimeoutStartSec=1200
TimeoutStopSec=70
ExecStartPre=/usr/bin/perl -e 'sleep 1 until -e "/sys/class/net/framaspace"'
ExecStartPre=/bin/rm -f %t/container-shared-collabora-%i.pid %t/container-shared-collabora-%i.ctr-id
ExecStart=/usr/bin/podman run \
    --rm \
    --rmi \
    --privileged \
    --conmon-pidfile %t/container-shared-collabora-%i.pid \
    --cidfile %t/container-shared-collabora-%i.ctr-id \
    --cgroups=no-conmon \
    --replace \
    --tty \
    --detach \
    --publish {{ ip }}:%i:9980 \
    --env "ALLOWED_HOST=https://demo\\.{{ escaped_fspace_domain }}:443" \
    --env "ALLOWED_ALIAS=https://\\.*\\\\.{{ escaped_fspace_domain_regex }}:443" \
    --env "PRESPAWN_CHILD_PROCESSES=2" \
    --env "ENABLE_CLEANUP=TRUE" \
    --env "ENABLE_ADMIN_CONSOLE=FALSE" \
    --env "REMOTE_FONT_URL=https://www.frama.space/collabora/fonts.json" \
    --env "FORCE_HOSTNAME=shared-{{ server_nb }}-%i.office.{{ fspace_domain }}" {%- if 'office' in pillar['framaspace'] and 'collabora' in pillar['framaspace']['office'] %} -e "LANGUAGE={{ pillar['framaspace']['office']['collabora']['language'] | default('en_GB en_US') }}"{%- endif %} \
    --env "INTERFACE=tabbed" {%- if 'office' in pillar['framaspace'] and 'collabora' in pillar['framaspace']['office'] %} -e "DICTIONARIES={{ pillar['framaspace']['office']['collabora']['dictionaries'] | default('') }}"{%- endif %} \
    --name shared-collabora-%i \
    framasoft/collabora-online-unleashed:24.04.9-1
ExecStop=/usr/bin/podman stop --ignore --cidfile %t/container-shared-collabora-%i.ctr-id -t 10
ExecStopPost=/usr/bin/podman rm --ignore -f --cidfile %t/container-shared-collabora-%i.ctr-id
PIDFile=%t/container-shared-collabora-%i.pid
Type=forking

[Install]
WantedBy=multi-user.target default.target
