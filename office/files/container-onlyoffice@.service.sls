[Unit]
Description=Podman container-onlyoffice-%i.service
Documentation=man:podman-generate-systemd(1)
Wants=network.target
After=network-online.target
Requires=wg-quick@framaspace.service
After=wg-quick@framaspace.service

# Attention !
# https://blog.ppom.me/2022-11-07-postmortem
# https://github.com/ONLYOFFICE/Docker-DocumentServer/pull/507
# https://github.com/ONLYOFFICE/Docker-DocumentServer/pull/545

[Service]
Environment=PODMAN_SYSTEMD_UNIT=%n
EnvironmentFile=/etc/default/onlyoffice
Restart=on-failure
TimeoutStartSec=1200
TimeoutStopSec=70
ExecStartPre=/usr/bin/perl -e 'sleep 1 until -e "/sys/class/net/framaspace"'
ExecStartPre=/bin/rm -f %t/container-onlyoffice-%i.pid %t/container-onlyoffice-%i.ctr-id
ExecStart=/usr/bin/podman run \
    --rm \
    --rmi \
    --conmon-pidfile %t/container-onlyoffice-%i.pid \
    --cidfile %t/container-onlyoffice-%i.ctr-id \
    --cgroups=no-conmon \
    --replace \
    --interactive \
    --tty \
    --detach \
    --publish {{ ip }}:%i:80 \
    --env JWT_ENABLED='true' \
    --env "JWT_SECRET=${SECRET_%i}" \
    --name=onlyoffice-%i \
    onlyoffice/documentserver:8.1.3
ExecStop=/usr/bin/podman exec onlyoffice-%i documentserver-prepare4shutdown.sh
ExecStop=/usr/bin/podman stop --ignore --cidfile %t/container-onlyoffice-%i.ctr-id -t 20
ExecStopPost=/usr/bin/podman rm --ignore -f --cidfile %t/container-onlyoffice-%i.ctr-id
PIDFile=%t/container-onlyoffice-%i.pid
Type=forking

[Install]
WantedBy=multi-user.target default.target
