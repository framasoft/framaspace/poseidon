{% from "framaspace/map.jinja" import hosts with context %}
/etc/systemd/system/container-collabora@.service:
  file.managed:
    - source: salt://framaspace/office/files/container-collabora@.service.sls
    - template: jinja
    - default:
      ip: {{ hosts[grains['id']]['ip'] }}
      fspace_domain: {{ pillar['framaspace']['domain'] }}
      escaped_fspace_domain: {{ pillar['framaspace']['domain'] | replace('.', '\\\\.') }}
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/container-collabora@.service
