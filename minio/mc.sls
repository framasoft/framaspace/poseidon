/opt/minio_home/bin/mcli:
  file.managed:
    - source: "https://dl.min.io/client/mc/release/linux-amd64/mc"
    - source_hash: "https://dl.min.io/client/mc/release/linux-amd64/mc.sha256sum"
    - source_hash_name: /opt/minio_home/bin/mcli
    - mode: 755
    - user: minio
    - group: minio
    - makedirs: True
  cmd.run:
    - name: "/opt/minio_home/bin/mcli alias set fspace https://minio.{{ pillar['framaspace']['domain'] }} '{{ pillar['framaspace']['minio']['root-user'] }}' '{{ pillar['framaspace']['minio']['root-password'] }}'"
    - runas: root
configure-alias-replicat:
  cmd.run:
    - name: "/opt/minio_home/bin/mcli alias set replicat https://minio-rep.{{ pillar['framaspace']['domain'] }} '{{ pillar['framaspace']['minio']['root-user'] }}' '{{ pillar['framaspace']['minio']['root-password'] }}'"
    - runas: root
remove-aliases:
  cmd.run:
    - name: "for i in s3 play local gcs; do /opt/minio_home/bin/mcli alias remove $i; done"
    - runas: root
/usr/local/bin/mcli:
  file.symlink:
    - target: /opt/minio_home/bin/mcli
