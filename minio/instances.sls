{% if grains['id'] == pillar['framaspace']['minio']['primary'] %}
manage-users-and-buckets:
  cmd.script:
    - name: salt://framaspace/minio/files/manage-users-and-buckets.sh.sls
    - source: salt://framaspace/minio/files/manage-users-and-buckets.sh.sls
    - template: jinja
manage-users-and-buckets-for-paheko:
  cmd.script:
    - name: salt://framaspace/minio/files/manage-users-and-buckets-for-paheko.sh.sls
    - source: salt://framaspace/minio/files/manage-users-and-buckets-for-paheko.sh.sls
    - template: jinja
{% endif %}
