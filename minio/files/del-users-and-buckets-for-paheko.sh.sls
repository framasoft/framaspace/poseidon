#!/bin/bash
# vim:set ft=bash:

{% set f = pillar['framaspace-jobs-delete'] %}
{% for instance in f.keys() %}
export INSTANCE=paheko-{{ instance }}

## User
# Unsetting the policy from the user
if [[ $(mcli --json admin user policy fspace "$INSTANCE" | jq .status -r) != 'error' ]]; then
    mcli admin policy detach fspace "rw-$INSTANCE" --user "$INSTANCE"
    sleep 1
fi

# Remove the user policy to access the bucket
if [[ $(mcli --json admin policy info fspace "rw-$INSTANCE" | jq .status -r) == 'success' ]]; then
    mcli admin policy remove fspace "rw-$INSTANCE"
    sleep 1
fi

# Delete the user
if [[ $(mcli --json admin user info fspace "$INSTANCE" | jq .status -r) == 'success' ]]; then
    mcli admin user remove fspace "$INSTANCE"
    sleep 1
fi

## Bucket
# Delete the bucket
if [[ $(mcli --json ls "fspace/$INSTANCE" | jq .status -r) != 'error' ]]; then
    RES=$(mcli --json rb --force "fspace/$INSTANCE" | jq .status -r)
    TRY=0
    # It fails sometimes due to the replication to the other cluster
    while [[ $RES == 'error' ]]; do
        sleep 1
        if [[ $(mcli --json ls "fspace/$INSTANCE" | jq .status -r) != 'error' ]]; then
            break
        else
            RES=$(mcli --json rb --force "fspace/$INSTANCE" | jq .status -r)
            TRY=$(( $TRY + 1 ))
        fi

        # Don’t try more than 10 times
        if [[ $TRY -gt 10 ]]; then
            break
        fi
    done
fi
{% endfor %}
