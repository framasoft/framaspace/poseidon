#!/bin/bash
# vim:set ft=bash:

check_ilm() {
    for SERVER in fspace replicat; do
        for ilmid in $(mcli --json ilm rule ls "$SERVER/$INSTANCE" | jq '.config.Rules | .[] | .ID' -r); do
            mcli --json ilm rule rm --id "$ilmid" "$SERVER/$INSTANCE"
        done
        mcli --json ilm rule add --noncurrent-expire-days 1 "$SERVER/$INSTANCE"
        mcli --json ilm rule add --expire-delete-marker     "$SERVER/$INSTANCE"
    done
}

{% set f        = pillar['framaspace'] %}
{% set m        = pillar['framaspace']['minio'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% do active.update(inactive) %}
{% for instance in active.keys() %}
{%   set quota = m['quota'] %}
export INSTANCE=nc-{{ instance }}

# Setting ILM
check_ilm

{% endfor %}
