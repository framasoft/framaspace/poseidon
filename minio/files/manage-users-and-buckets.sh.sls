#!/bin/bash
# vim:set ft=bash:
{% set f        = pillar['framaspace'] %}
{% set m        = pillar['framaspace']['minio'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% do active.update(inactive) %}

check_ilm() {
    for SERVER in fspace replicat; do
        ILM=$(mcli --json ilm rule ls "$SERVER/$INSTANCE")
        if [[ $(echo "$ILM" | jq .status -r) == 'success' ]]; then
            for ilmid in $(echo "$ILM" | jq '.config.Rules | .[] | .ID' -r); do
                mcli --json ilm rule rm --id "$ilmid" "$SERVER/$INSTANCE"
            done
        fi
        mcli ilm rule add --noncurrent-expire-days 1 "$SERVER/$INSTANCE"
        mcli ilm rule add --expire-delete-marker     "$SERVER/$INSTANCE"
    done
}

{% for instance in active.keys() %}
{%   set pwd   = '%s-%s' % (instance, f['minio']['password-seed']) %}
{%   set quota = m['quota'] %}
export INSTANCE=nc-{{ instance }}
export PASSWD={{ pwd | sha256 }}

## Bucket
# Creation
if [[ $(mcli --json ls "fspace/$INSTANCE" | jq .status -r) == 'error' ]]; then
    mcli mb "fspace/$INSTANCE"
    sleep 1
    if [[ $(mcli --json ls "fspace/$INSTANCE" | jq .status -r) == 'error' ]]; then
        sleep 3
        if [[ $(mcli --json ls "fspace/$INSTANCE" | jq .status -r) == 'error' ]]; then
            mcli mb "fspace/$INSTANCE"
            sleep 1
        fi
    fi
fi
# Enabling versioning
if [[ -z $(mcli --json version info "fspace/$INSTANCE" | jq .versioning.status -r) ]]; then
    mcli version enable "fspace/$INSTANCE"
    sleep 1
fi
# Setting ILM
check_ilm
# Setting quota
if [[ $(mcli --json quota info "fspace/$INSTANCE" | jq .type -r ) == 'null' ]]; then
    mcli quota set "fspace/$INSTANCE" --size {{ quota }}iB
    sleep 1
elif [[ $(mcli --json quota info "fspace/$INSTANCE" | jq .quota -r | numfmt --to iec-i) != '{{ quota }}i' ]]; then
    mcli quota set "fspace/$INSTANCE" --size {{ quota }}iB
    sleep 1
fi

## User
# Creation
if [[ $(mcli --json admin user info fspace "$INSTANCE" | jq .status -r) == 'error' ]]; then
    mcli admin user add fspace "$INSTANCE" "$PASSWD"
    sleep 1
    if [[ $(mcli --json admin user info fspace "$INSTANCE" | jq .status -r) == 'error' ]]; then
        sleep 3
        if [[ $(mcli --json admin user info fspace "$INSTANCE" | jq .status -r) == 'error' ]]; then
            mcli admin user add fspace "$INSTANCE" "$PASSWD"
        fi
    fi
fi
# Creation of the user policy to access the bucket
if [[ $(mcli --json admin policy info fspace "rw-$INSTANCE" | jq .status -r) == 'error' ]]; then
    cat <<EOF > "/tmp/$INSTANCE.json"
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "arn:aws:s3:::$INSTANCE/*"
            ]
        }
    ]
}
EOF

    RES=$(mcli --json admin policy create fspace "rw-$INSTANCE" "/tmp/$INSTANCE.json" | jq .status -r)
    TRY=0
    # It fails sometimes due to the replication to the other cluster
    while [[ $RES == 'error' ]]; do
        sleep 1
        if [[ $(mcli --json admin policy create fspace "rw-$INSTANCE" "/tmp/$INSTANCE.json" | jq .status -r) != 'error' ]]; then
            break
        else
            RES=$(mcli --json admin policy create fspace "rw-$INSTANCE" "/tmp/$INSTANCE.json" | jq .status -r)
            TRY=$(( $TRY + 1 ))
        fi

        # Don’t try more than 10 times
        if [[ $TRY -gt 10 ]]; then
            break
        fi
    done
    rm "/tmp/$INSTANCE.json"
    sleep 1
fi
# Setting the policy for the user
if [[ $(mcli --json admin user policy fspace "$INSTANCE" | jq .status -r) == 'error' ]]; then
    mcli admin policy attach fspace "rw-$INSTANCE" --user "$INSTANCE"
fi

{% endfor %}
