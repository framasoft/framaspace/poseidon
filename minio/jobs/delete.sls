{% if grains['id'] == pillar['framaspace']['minio']['primary'] %}
del-users-and-buckets:
  cmd.script:
    - name: salt://framaspace/minio/files/del-users-and-buckets.sh.sls
    - source: salt://framaspace/minio/files/del-users-and-buckets.sh.sls
    - template: jinja
del-users-and-buckets-for-paheko:
  cmd.script:
    - name: salt://framaspace/minio/files/del-users-and-buckets-for-paheko.sh.sls
    - source: salt://framaspace/minio/files/del-users-and-buckets-for-paheko.sh.sls
    - template: jinja
{% endif %}
