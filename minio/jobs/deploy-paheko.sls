{% if grains['id'] == pillar['framaspace']['minio']['primary'] %}
add-users-and-buckets-for-paheko:
  cmd.script:
    - name: salt://framaspace/minio/files/add-users-and-buckets-for-paheko.sh.sls
    - source: salt://framaspace/minio/files/add-users-and-buckets-for-paheko.sh.sls
    - template: jinja
    - defaults:
      job: 'framaspace-jobs-deploy-paheko'
{% endif %}
