/opt/minio_home/bin/minio:
  pkg.installed:
    - pkgs:
      - xfsprogs
      - fdisk
  user.present:
    - name: minio
    - shell: /bin/false
    - home: /opt/minio_home
    - createhome: True
    - system: True
  alias.present:
    - name: minio
    - target: root
  file.managed:
    - source: "https://dl.min.io/server/minio/release/linux-amd64/minio"
    - source_hash: "https://dl.min.io/server/minio/release/linux-amd64/minio.sha256sum"
    - source_hash_name: /opt/minio_home/bin/bin/minio
    - makedirs: True
    - mode: 755
    - user: minio
    - group: minio

# You need to manually create the partitions (use xfs) and mount them
{% for i in [1, 2, 3, 4] %}
/mnt/data{{ i }}/minio-storage:
  file.directory:
    - user: minio
    - group: minio
    - mode: 755
{% endfor %}
