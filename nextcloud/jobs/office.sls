{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-office'] %}
{% set fi     = pillar['framaspace-instances'] | default({}) %}
{% set active = fi['active'] | default({}) %}

{% if grains['id'] == f['nginx']['primary'] %}
{%   for i, data in j.items() %}
{#     Update active pillar for /opt/check-onlyoffice-connection-instances.txt #}
{%     do active[i].update({'office_type': data['office_type']}) %}

# Deactivate old office backend
{%     if data['old_office_type'] == 10 or data['old_office_type'] == 30 %}
# Collabora
disable-collabora-{{ i }}:
  cmd.run:
    - name: php occ app:disable richdocuments
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{%     elif data['old_office_type'] == 20 %}
# Onlyoffice
disable-onlyoffice-{{ i }}:
  cmd.run:
    - name: php occ app:disable onlyoffice
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{%     endif %}

# Activate new office backend
{%     if data['office_type'] == 10 or data['office_type'] == 30 %}
# Collabora
enable-richdocuments-{{ i }}:
  cmd.run:
    - name: php occ app:enable richdocuments
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}

{%       set office_server = i %}
{%       if data['office_type'] == 30 %}
{%         set office_server = 'shared-%s-%i' % (data['office_server'].replace('office-', ''), data['office_port']) %}
{%       endif %}
config-richdocuments-wopi_url-{{ i }}:
  cmd.run:
    - name: php occ config:app:set richdocuments wopi_url --value 'https://{{ office_server }}.office.{{ f['domain'] }}'
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
config-richdocuments-public_wopi_url-{{ i }}:
  cmd.run:
    - name: php occ config:app:set richdocuments public_wopi_url --value 'https://{{ office_server }}.office.{{ f['domain'] }}:443'
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
config-richdocuments-activate-{{ i }}:
  cmd.run:
    - name: php occ richdocuments:activate-config
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{%     elif data['office_type'] == 20 %}
# Onlyoffice
enable-onlyoffice-{{ i }}:
  cmd.run:
    - name: php occ app:enable onlyoffice
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
config-onlyoffice-activate-{{ i }}:
  cmd.run:
    - name: php occ onlyoffice:documentserver --check
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{%     endif %}
{%   endfor %}
{% endif %}

/opt/check-onlyoffice-connection-instances.txt:
  file.managed:
    - user: www-data
    - group: www-data
    - contents:
{% for k, v in active.items() -%}
{%-   if v['office_type'] == 20 %}
      - {{ k }}{%- endif %}
{% endfor %}
