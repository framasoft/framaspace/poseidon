{% from "framaspace/map.jinja" import hosts with context %}
{% set f       = pillar['framaspace'] %}
{% set j       = pillar['framaspace-jobs-deploy-paheko'] %}
{% for i, data in j.items() %}
{%   if grains['id'] == f['nginx']['primary'] %}
# Paheko
paheko-shared-key-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace api_shared_key --value="{{ data['paheko_shared_secret_key'] }}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
paheko-api-user-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace paheko_api_user --value="{{ data['paheko_api_user'] }}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
paheko-api-password-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace paheko_api_password --value="{{ data['paheko_api_password'] }}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
paheko-url-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace paheko_url --value="https://{{ i }}.paheko.{{ f['domain']}}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
enable-paheko-{{ i }}:
  cmd.run:
    - name: php occ app:enable paheko_fspace
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%   endif %}
{% endfor %}
