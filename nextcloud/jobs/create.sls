{% from "framaspace/map.jinja" import hosts with context %}
{% set f       = pillar['framaspace'] %}
{% set j       = pillar['framaspace-jobs-create'] %}
{% set fi      = pillar['framaspace-instances'] | default({}) %}
{% set active  = fi['active'] | default({}) %}
{% set a       = f['nextcloud']['apps'].keys() | list %}
{% set b       = f['nextcloud']['disabled-apps'].keys() | list %}
{% set apps    = a + b %}
{% set redis_s = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
{% for i, data in j.items() %}
{%   set dbpwd = '%s-%s' % (i, f['pg']['password-seed']) %}
{%   set mpwd  = '%s-%s' % (i, f['minio']['password-seed']) %}

{#   Update active pillar for /opt/check-onlyoffice-connection-instances.txt #}
{%   do active.update({i: data}) %}

/var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/:
  file.directory:
    - makedirs: True
    - user: www-data
    - group: www-data

/var/www/nc-instances/{{ i }}.{{ f['domain'] }}:
  file.symlink:
    - target: /var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}/
    - user: www-data
    - group: www-data
    - makedirs: True

# Instance’s prod config
config-nc-{{ i }}-prod:
  file.managed:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.conf.prod.php
    - source: salt://framaspace/nextcloud/files/nextcloud-instance-config.php.sls
    - makedirs: True
    - user: www-data
    - group: www-data
    - mode: 640
    - template: jinja
    - default:
      instance: {{ i }}
      instanceid: {{ data['nc_instanceid'] }}
      passwordsalt: {{ data['nc_passwordsalt'] }}
      secret: {{ data['nc_secret'] }}
      domain: {{ f['domain'] }}
      dbpassword: {{ dbpwd | sha256 }}
      miniopassword: {{ mpwd | sha256 }}
      appstoreenabled: 'false'
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}

# Instance’s maintenance config with 'maintenance' => false
# to let us configure the apps
config-nc-{{ i }}-maintenance-install:
  file.managed:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.conf.maintenance.php
    - source: salt://framaspace/nextcloud/files/nextcloud-instance-config.php.sls
    - makedirs: True
    - user: www-data
    - group: www-data
    - mode: 640
    - template: jinja
    - default:
      instance: {{ i }}
      instanceid: {{ data['nc_instanceid'] }}
      passwordsalt: {{ data['nc_passwordsalt'] }}
      secret: {{ data['nc_secret'] }}
      domain: {{ f['domain'] }}
      dbpassword: {{ dbpwd | sha256 }}
      miniopassword: {{ mpwd | sha256 }}
      dbhost: "{{ hosts[f['pg']['primary']]['ip'] }}"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
      maintenance: False

# Keep instance in maintenance for now
symlink-for-installation-{{ i }}:
  file.symlink:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.conf.php
    - target: {{ i }}.conf.maintenance.php
    - user: www-data
    - group: www-data
    - makedirs: True
    - force: True

{%   if grains['id'] == f['nginx']['primary'] %}
# Instance’s install config
config-nc-{{ i }}-installation:
  file.managed:
    - name: /var/www/installation/nextcloud/config/config.php
    - source: salt://framaspace/nextcloud/files/nextcloud-install-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      instance: {{ i }}
      domain: {{ f['domain'] }}
      version: {{ f['nextcloud']['version_for_config'] }}
      apps: "'{{ apps | join("', '") }}'"
      redis_s: "'{{ redis_s | join("', '") }}'"
      miniopassword: {{ mpwd | sha256 }}
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
install-skeleton-{{ i }}:
  file.directory:
    - name: /opt/demeter/install-skeleton
    - user: www-data
    - group: www-data

# Instance installation
install-nc-{{ i }}:
  cmd.run:
    - cwd: /var/www/installation/nextcloud/
    - runas: www-data
    - name: php occ maintenance:install --database=pgsql --database-host={{ hosts[f['pg']['primary']]['ip'] }}:5432 --database-name='nc-{{ i }}' --database-user='{{ i }}' --database-pass='{{ dbpwd | sha256 }}' --admin-user='{{ data['nc_remote_admin_username'] }}' --admin-pass='{{ data['nc_remote_admin_password'] }}' --admin-email='{{ data['nc_remote_admin_email'] }}' --data-dir=/var/www/installation/nextcloud-data && touch /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
    - creates: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
    # ' juste pour retablir la coloration syntaxique du fichier

# Necessary corrections
mimetypes-nc-{{ i }}:
  cmd.run:
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - name: php occ maintenance:mimetype:update-db; php occ maintenance:mimetype:update-db --repair-filecache && php occ maintenance:mimetype:update-js && touch /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.db-updated
    - creates: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.db-updated
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Apps downloading
{%     for app in apps %}
download-{{ app }}-{{ i }}:
  cmd.run:
    - name: php occ app:install --keep-disabled {{ app }}
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - creates: /var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}/apps/{{ app }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     endfor %}
{%     for app in f['nextcloud']['apps'].keys() %}
enable-{{ app }}-{{ i }}:
  cmd.run:
    - name: php occ app:enable {{ app }}
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     endfor %}
{%     if 'native-apps-to-enable' in f['nextcloud'] %}
{%       for app in f['nextcloud']['native-apps-to-enable'] %}
enable-{{ app }}-{{ i }}:
  cmd.run:
    - name: php occ app:enable {{ app }}
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%       endfor %}
{%     endif %}
{%     if 'native-apps-to-disable' in f['nextcloud'] %}
{%       for app in f['nextcloud']['native-apps-to-disable'] %}
disable-{{ app }}-{{ i }}:
  cmd.run:
    - name: php occ app:disable {{ app }}
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%       endfor %}
{%     endif %}

# Disable activities notifications for nc_remote_admin
{%     for setting in ['notify_setting_batchtime', 'activity_digest', 'notify_email_group_settings'] %}
set-activity-{{ setting }}-{{ i }}:
  cmd.run:
    - name: php occ user:setting {{ data['nc_remote_admin_username'] }} activity {{ setting }} 0
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     endfor %}

# Global quota
set-global-quota-{{ i }}:
  cmd.run:
    - name: php occ config:app:set core quota_instance_global --value {{ f['nextcloud']['quota'] | human_to_bytes }}
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Apps settings
{%     for app, settings in f['nextcloud']['apps-settings'].items() %}
{%       for setting, value in settings.items() %}
config-{{ app }}-{{ setting }}-{{ i }}:
  cmd.run:
    - name: php occ config:app:set {{ app }} {{ setting }} --value='{{ value }}'
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%       endfor %}
{%     endfor %}
# ' just to get back syntaxic coloration in Vim

# App Collabora
config-richdocuments-wopi_url-{{ i }}:
  cmd.run:
    - name: php occ config:app:set richdocuments wopi_url --value 'https://{{ i }}.office.{{ f['domain'] }}'
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
config-richdocuments-public_wopi_url-{{ i }}:
  cmd.run:
    - name: php occ config:app:set richdocuments public_wopi_url --value 'https://{{ i }}.office.{{ f['domain'] }}:443'
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# App Onlyoffice
config-onlyoffice-jwt-{{ i }}:
  cmd.run:
    - name: php occ config:app:set onlyoffice jwt_secret --value {{ data['oo_secret'] }}
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
config-onlyoffice-server-{{ i }}:
  cmd.run:
    - name: php occ config:app:set onlyoffice DocumentServerUrl --value 'https://{{ i }}.office.{{ f['domain'] }}'
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     for setting in ['defFormats', 'editFormats'] %}
config-onlyoffice-{{ setting }}-{{ i }}:
  cmd.run:
    - name: JSON=$(sed -e "s/ //g" /opt/demeter/onlyoffice/{{ setting }}.json | tr -d '\n') && php occ config:app:set onlyoffice {{ setting }} --value "$JSON"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     endfor %}

# Office backend’s activation
{%     if data['office_type'] == 10 or data['office_type'] == 30 %}
enable-richdocuments-{{ i }}:
  cmd.run:
    - name: php occ app:enable richdocuments
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
config-richdocuments-activate-{{ i }}:
  cmd.run:
    - name: php occ richdocuments:activate-config
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     elif data['office_type'] == 20 %}
enable-onlyoffice-{{ i }}:
  cmd.run:
    - name: php occ app:enable onlyoffice
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
config-onlyoffice-activate-{{ i }}:
  cmd.run:
    - name: php occ onlyoffice:documentserver --check
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     endif %}

{%     for setting in ['logo', 'background'] %}
{%       set ext = 'jpg' %}
{%       if setting == 'logo' %}
{%         set ext = 'png' %}
{%       endif %}
# Set {{ setting }} in theming app
set-{{ setting }}-{{ i }}:
  cmd.run:
    - name: php occ theming:config {{ setting }} /opt/demeter/theming/{{ setting }}.{{ ext }}
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     endfor %}

# Set instance’s name
set-instance-name-{{ i }}:
  cmd.run:
    - name: php occ theming:config name "{{ f['nextcloud']['instance-name-template'] | replace('__i__', i[0] | upper ~ i[1:]) }}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Set custom CSS
set-custom-css-{{ i }}:
  cmd.run:
    - name: CSS=$(cat /opt/demeter/theming_customcss/hide-settings.css) && php occ config:app:set theming_customcss customcss --value "$CSS"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Set external sites
set-external-sites-{{ i }}:
  cmd.run:
    - name: SITES=$(cat /opt/demeter/external/sites.json | tr "\n" " " | sed -e "s@ \+@ @g" -e "s@/@\\\\/@g") && php occ config:app:set external sites --value "$SITES"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Paheko
{%     if 'paheko_shared_server' in data %}
paheko-shared-key-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace api_shared_key --value="{{ data['paheko_shared_secret_key'] }}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
paheko-api-user-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace paheko_api_user --value="{{ data['paheko_api_user'] }}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
paheko-api-password-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace paheko_api_password --value="{{ data['paheko_api_password'] }}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
paheko-url-{{ i }}:
  cmd.run:
    - name: php occ config:app:set paheko_fspace paheko_url --value="https://{{ i }}.paheko.{{ f['domain']}}"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
enable-paheko-{{ i }}:
  cmd.run:
    - name: php occ app:enable paheko_fspace
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%     endif %}

# Insert TOS in database
add-TOS-{{ i }}:
  cmd.script:
    - name: salt://framaspace/nextcloud/files/set_tos.py.sls
    - source: salt://framaspace/nextcloud/files/set_tos.py.sls
    - template: jinja
    - default:
      domain: {{ i }}
      dbpwd: {{ dbpwd | sha256 }}
      dbhost: "{{ hosts[f['pg']['primary']]['ip'] }}"
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Run first cron
run-first-cron-{{ i }}:
  cmd.run:
    - name: php cron.php
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Reset install folder configuration
reset-install-config-after-{{ i }}-installation:
  file.managed:
    - name: /var/www/installation/nextcloud/config/config.php
    - source: salt://framaspace/nextcloud/files/nextcloud-install-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      apps: "'{{ apps | join("', '") }}'"
      domain: {{ f['domain'] }}
      version: {{ f['nextcloud']['version_for_config'] }}
      redis_s: "'{{ redis_s | join("', '") }}'"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}

delete-nc-data-{{ i }}:
  file.absent:
    - name: /var/www/installation/nextcloud-data

# Put instance in production now
/var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.conf.php:
  file.symlink:
    - target: {{ i }}.conf.prod.php
    - user: www-data
    - group: www-data
    - makedirs: True
    - force: True
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed

# Instance’s maintenance config with 'maintenance' => true
config-nc-{{ i }}-real-maintenance:
  file.managed:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.conf.maintenance.php
    - source: salt://framaspace/nextcloud/files/nextcloud-instance-config.php.sls
    - makedirs: True
    - user: www-data
    - group: www-data
    - mode: 640
    - template: jinja
    - default:
      instance: {{ i }}
      instanceid: {{ data['nc_instanceid'] }}
      passwordsalt: {{ data['nc_passwordsalt'] }}
      secret: {{ data['nc_secret'] }}
      domain: {{ f['domain'] }}
      dbpassword: {{ dbpwd | sha256 }}
      miniopassword: {{ mpwd | sha256 }}
      dbhost: "{{ hosts[f['pg']['primary']]['ip'] }}"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
      maintenance: True

temp-sign-of-tos-for-admin-{{ i }}:
  cmd.run:
    - name: psql -f /opt/demeter/sql/accept-tos-up.sql
    - env:
      - PGDATABASE: nc-{{ i }}
      - PGUSER: {{ i }}
      - PGPASSWORD: {{ dbpwd | sha256 }}
{%   endif %}
{% endfor %}

/opt/check-onlyoffice-connection-instances.txt:
  file.managed:
    - user: www-data
    - group: www-data
    - contents:
{% for k, v in active.items() -%}
{%-   if v['office_type'] == 20 %}
      - {{ k }}{%- endif %}
{% endfor %}
