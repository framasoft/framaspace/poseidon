{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-app-password'] %}
{% for i, data in j.items() %}
{%   if grains['id'] == f['nginx']['primary'] %}
create-app-password-{{ i }}:
  cmd.run:
    - name: php occ user:add-app-password admin -n --no-warnings --no-ansi | tail -n 1
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain'] }}/{{ i }}.installed
{%   endif %}
{% endfor %}
