{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-delete'] %}

{% for i in j.keys() %}
delete-config-directory-{{ i }}:
  file.absent:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/
delete-disabled-config-directory-{{ i }}:
  file.absent:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}.disabled/
delete-symlink-nc-{{ i }}:
  file.absent:
    - name: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}
    - onlyif:
      - test -e /var/www/nc-instances/{{ i }}.{{ f['domain'] }}
{% endfor %}
