{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-disable'] %}
{% set fi     = pillar['framaspace-instances'] | default({}) %}
{% set active = fi['active'] | default({}) %}

{% for i, data in j.items() %}

{#   Update active pillar for /opt/check-onlyoffice-connection-instances.txt #}
{%   do active.pop(i) %}

disable-config-nc-{{ i }}:
  file.rename:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}.disabled
    - source: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}
    - force: True
{% endfor %}

/opt/check-onlyoffice-connection-instances.txt:
  file.managed:
    - user: www-data
    - group: www-data
    - contents:
{% for k, v in active.items() -%}
{%-   if v['office_type'] == 20 %}
      - {{ k }}{%- endif %}
{% endfor %}
