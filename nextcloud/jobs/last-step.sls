{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-last-step'] %}
{% for i, data in j.items() %}
{%   if grains['id'] == f['nginx']['primary'] %}
{%   set dbpwd = '%s-%s' % (i, f['pg']['password-seed']) %}
rm-temp-sign-of-tos-for-admin-{{ i }}:
  cmd.run:
    - name: psql -f /opt/demeter/sql/accept-tos-down.sql
    - env:
      - PGDATABASE: nc-{{ i }}
      - PGUSER: {{ i }}
      - PGPASSWORD: {{ dbpwd | sha256 }}
truncate-activities-{{ i }}:
  cmd.run:
    - name: psql -f /opt/demeter/sql/truncate-activities.sql
    - env:
      - PGDATABASE: nc-{{ i }}
      - PGUSER: {{ i }}
      - PGPASSWORD: {{ dbpwd | sha256 }}
{%   endif %}
{% endfor %}
