{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-create'] %}

{% if grains['id'] == f['nginx']['primary'] %}
{%   for i in j.keys() %}
add-{{ i }}-to-chronos:
  cmd.run:
    - name: psql -c "INSERT INTO domains (domain) VALUES ('{{ i }}') ON CONFLICT DO NOTHING" {{ f['chronos']['db']['name'] }}
    - env:
      - PGUSER: {{ f['chronos']['db']['user'] }}
      - PGPASSWORD:  {{ f['chronos']['db']['password'] }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%   endfor %}
{% endif %}
