{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-reenable'] %}

{% if grains['id'] == f['nginx']['primary'] %}
{%   for i in j.keys() %}
add-{{ i }}-to-chronos:
  cmd.run:
    - name: psql -c "INSERT INTO domains (domain) VALUES ('{{ i }}') ON CONFLICT DO NOTHING" {{ f['chronos']['db']['name'] }}
    - env:
      - PGUSER: {{ f['chronos']['db']['user'] }}
      - PGPASSWORD:  {{ f['chronos']['db']['password'] }}
{%   endfor %}
{% endif %}
