{% set f = pillar['framaspace'] %}
{% set j = pillar['framaspace-jobs-disable'] %}

{% if grains['id'] == f['nginx']['primary'] %}
{%   for i in j.keys() %}
delete-{{ i }}-from-chronos:
  cmd.run:
    - name: psql -c "DELETE FROM domains WHERE domain = '{{ i }}'" {{ f['chronos']['db']['name'] }}
    - env:
      - PGUSER: {{ f['chronos']['db']['user'] }}
      - PGPASSWORD:  {{ f['chronos']['db']['password'] }}
{%   endfor %}
{% endif %}
