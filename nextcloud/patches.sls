patches:
  git.latest:
    - name: https://framagit.org/framasoft/framaspace/asclepios.git
    - target: /opt/asclepios
    - user: www-data
    - branch: {{ pillar['framaspace']['nextcloud']['version'] }}
    - force_reset: True
    - force_fetch: True
  cmd.script:
    - name: salt://framaspace/nextcloud/files/apply-patches.sh.sls
    - source: salt://framaspace/nextcloud/files/apply-patches.sh.sls
    - runas: www-data
    - template: jinja
    - defaults:
      nc_dir: /var/www/nc/nextcloud-{{ pillar['framaspace']['nextcloud']['version'] }}/
