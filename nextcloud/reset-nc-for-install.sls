{% from "framaspace/map.jinja" import hosts with context %}
{% set f = pillar['framaspace'] %}
{% set redis_s = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
reset-template-config-after-installation:
  file.managed:
    - name: /var/www/installation/nextcloud/config/config.php
    - source: salt://framaspace/nextcloud/files/nextcloud-install-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      domain: {{ f['domain'] }}
      version: {{ f['nextcloud']['version_for_config'] }}
      redis_s: "'{{ redis_s | join("', '") }}'"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
/var/www/installation/nextcloud-data:
  file.absent
