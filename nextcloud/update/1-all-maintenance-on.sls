{% set f           = pillar['framaspace'] %}
{% set i           = pillar['instance_to_upgrade'] %}
{% set new_version = f['nextcloud']['version'] %}

{% if f['nextcloud']['extra_pre_upgrade_commands'] is defined %}
{%   for command in f['nextcloud']['extra_pre_upgrade_commands'] %}
extra-pre-command-{{ command | replace(' ', '-') | replace(':', '-') }}-{{ i }}:
  cmd.run:
    - name: {{ command }}
    - runas: www-data
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}
    - env:
      - NC_INSTANCE: {{ i }}
{%   endfor %}
{% endif %}

{% if salt['file.readlink']('/var/www/nc-instances/' ~ i ~ '.' ~ f['domain']) != '/var/www/nc/nextcloud-' ~ new_version ~ '/' %}
symlink-for-maintenance-{{ i }}:
  file.symlink:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.conf.php
    - target: {{ i }}.conf.maintenance.php
    - user: www-data
    - group: www-data
    - makedirs: True
    - force: True
{% else %}
notice-{{ i }}:
  cmd.run:
    - name: echo "{{ i }} est déjà en version {{ new_version }}"
{% endif %}
