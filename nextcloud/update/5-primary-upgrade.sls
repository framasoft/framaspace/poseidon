{% from "framaspace/map.jinja" import hosts with context %}
{% set f        = pillar['framaspace'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% set a        = f['nextcloud']['apps'].keys() | list %}
{% set b        = f['nextcloud']['disabled-apps'].keys() | list %}
{% set apps     = a + b %}
{% set redis_s  = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
{% set i = pillar['instance_to_upgrade'] %}
{% set t = pillar['is_test_instance'] %}
{% if i in active %}
{%   set data = active[i] %}
{% else %}
{%   set data = inactive[i] | default({}) %}
{% endif %}
{% set new_version = f['nextcloud']['version'] %}
{% set dbpwd       = '%s-%s' % (i, f['pg']['password-seed']) %}
{% set mpwd        = '%s-%s' % (i, f['minio']['password-seed']) %}
{% set office_type = data['office_type'] | default(0)%}

{% if grains['id'] == f['nginx']['primary'] %}
{%   if salt['file.readlink']('/var/www/nc-instances/' ~ i ~ '.' ~ f['domain']) != '/var/www/nc/nextcloud-' ~ new_version ~ '/' %}
# Write a one-file config
unified-nextcloud-config:
  file.managed:
    - name: /var/www/nc/nextcloud-{{ new_version }}-dev/config/config.php
    - source: salt:///framaspace/nextcloud/update/files/nextcloud-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      apps: "'{{ apps | join("', '") }}'"
      old_version: {{ f['nextcloud']['version_for_config'] }}
      instance: {{ i }}
      instanceid: {{ data['nc_instanceid'] }}
      passwordsalt: {{ data['nc_passwordsalt'] }}
      secret: {{ data['nc_secret'] }}
      domain: {{ f['domain'] }}
      dbpassword: {{ dbpwd | sha256 }}
      miniopassword: {{ mpwd | sha256 }}
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
      redis_s: "'{{ redis_s | join("', '") }}'"

{%     if t is defined and t %}
unified-nextcloud-config-2:
  file.managed:
    - name: /tmp/config.php
    - source: salt:///framaspace/nextcloud/update/files/nextcloud-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      apps: "'{{ apps | join("', '") }}'"
      old_version: {{ f['nextcloud']['version_for_config'] }}
      instance: {{ i }}
      instanceid: {{ data['nc_instanceid'] }}
      passwordsalt: {{ data['nc_passwordsalt'] }}
      secret: {{ data['nc_secret'] }}
      domain: {{ f['domain'] }}
      dbpassword: {{ dbpwd | sha256 }}
      miniopassword: {{ mpwd | sha256 }}
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
      redis_s: "'{{ redis_s | join("', '") }}'"
{%     endif %}

# Upgrade db for core changes
upgrade-{{ i }}:
  cmd.run:
    - name: php occ upgrade
    - runas: www-data
    - cwd: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - env:
      - NC_INSTANCE: {{ i }}

# Enable each active app to update db if necessary
{%     for app in a %}
enable-{{ app }}-{{ i }}:
  cmd.run:
    - name: php occ app:enable {{ app }}
    - runas: www-data
    - cwd: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - env:
      - NC_INSTANCE: {{ i }}
{%     endfor %}

# Same for office backend’s
{%     if office_type == 10 or office_type == 30 %}
enable-richdocuments-{{ i }}:
  cmd.run:
    - name: php occ app:enable richdocuments
    - cwd: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{%     elif office_type == 20 %}
enable-onlyoffice-{{ i }}:
  cmd.run:
    - name: php occ app:enable onlyoffice
    - cwd: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{%     endif %}

{%     if 'native-apps-to-enable' in f['nextcloud'] %}
{%       for app in f['nextcloud']['native-apps-to-enable'] %}
enable-{{ app }}-{{ i }}:
  cmd.run:
    - name: php occ app:enable {{ app }}
    - cwd: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%       endfor %}
{%     endif %}
{%     if 'native-apps-to-disable' in f['nextcloud'] %}
{%       for app in f['nextcloud']['native-apps-to-disable'] %}
disable-{{ app }}-{{ i }}:
  cmd.run:
    - name: php occ app:disable {{ app }}
    - cwd: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%       endfor %}
{%     endif %}
{%     if f['nextcloud']['extra_upgrade_commands'] is defined %}
{%       for command in f['nextcloud']['extra_upgrade_commands'] %}
extra-command-{{ command | replace(' ', '-') | replace(':', '-') }}-{{ i }}:
  cmd.run:
    - name: {{ command }}
    - runas: www-data
    - cwd: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - env:
      - NC_INSTANCE: {{ i }}
{%       endfor %}
{%     endif %}

{%     if t is defined and t %}
# Check if php occ upgrade has made change to the config file (new keys, for ex)
diff-unified-config:
  cmd.run:
    - name: "diff --ignore-space-change -u /tmp/config.php /var/www/nc/nextcloud-{{ new_version }}-dev/config/config.php; exit 0"
{%     endif %}

{%   else %}
notice-{{ i }}:
  cmd.run:
    - name: echo "{{ i }} est déjà en version {{ new_version }}"
{%   endif %}
{% endif %}
