{% from "framaspace/map.jinja" import hosts with context %}
{% set f        = pillar['framaspace'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% set a        = f['nextcloud']['apps'].keys() | list %}
{% set b        = f['nextcloud']['disabled-apps'].keys() | list %}
{% set apps     = a + b %}
{% set redis_s  = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
{% set i           = pillar['instance_to_upgrade'] %}
{% set data        = active[i] %}
{% set new_version = f['nextcloud']['version'] %}
{% set dbpwd       = '%s-%s' % (i, f['pg']['password-seed']) %}
{% set mpwd        = '%s-%s' % (i, f['minio']['password-seed']) %}
{% set office_type = fi['active'][i]['office_type'] %}

{% if not salt['file.directory_exists']('/var/www/nc/nextcloud-' ~ new_version) %}
patches:
  git.latest:
    - name: https://framagit.org/framasoft/framaspace/asclepios.git
    - target: /opt/asclepios
    - user: www-data
    - rev: {{ new_version }}
    - branch: {{ new_version }}
    - force_reset: True
    - force_fetch: True
  cmd.script:
    - name: salt://framaspace/nextcloud/files/apply-patches.sh.sls
    - source: salt://framaspace/nextcloud/files/apply-patches.sh.sls
    - runas: www-data
    - template: jinja
    - defaults:
      nc_dir: /var/www/nc/nextcloud-{{ new_version }}-dev/

cp-new-nc-to-prod:
  file.copy:
    - name: /var/www/nc/nextcloud-{{ new_version }}
    - source: /var/www/nc/nextcloud-{{ new_version }}-dev/
    - preserve: True
    - user: www-data
    - group: www-data
fix-owner:
  file.directory:
    - name: /var/www/nc/nextcloud-{{ new_version }}
    - user: www-data
    - group: www-data

# Common config for production NC
create-prod-config-and-update-version:
  file.managed:
    - name: /var/www/nc/nextcloud-{{ new_version }}/config/config.php
    - source: salt://framaspace/nextcloud/files/nextcloud-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      domain: {{ f['domain'] }}
      version: {{ f['nextcloud']['version_for_config'] }}
      apps: "'{{ apps | join("', '") }}'"
      installation: False
      redis_s: "'{{ redis_s | join("', '") }}'"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
  cmd.script:
    - source: salt://framaspace/nextcloud/update/files/update-version-in-config.sh.sls
    - template: jinja
    - cwd: /var/www
    - runas: www-data
    - default:
      nc_folder: /var/www/nc/nextcloud-{{ new_version }}/
      instance: {{ i }}
      version: {{ new_version }}

{% endif %}
