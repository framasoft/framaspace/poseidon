{% from "framaspace/map.jinja" import hosts with context %}
{% set f        = pillar['framaspace'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% set a        = f['nextcloud']['apps'] %}
{% set b        = f['nextcloud']['disabled-apps'] %}
{% set e        = f['nextcloud']['external-apps'] | default({}) %}
{% set redis_s  = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
{% set i           = pillar['instance_to_upgrade'] %}
{% set data        = active[i] %}
{% set new_version = f['nextcloud']['version'] %}
{% set dbpwd       = '%s-%s' % (i, f['pg']['password-seed']) %}
{% set mpwd        = '%s-%s' % (i, f['minio']['password-seed']) %}
{% set office_type = fi['active'][i]['office_type'] %}

{% do a.update(b) %}

{% if not salt['file.directory_exists']('/var/www/nc/nextcloud-' ~ new_version) %}
update-apps-code:
  pkg.installed:
    - pkgs:
      - curl
      - jq
  cmd.script:
    - source: salt:///framaspace/nextcloud/update/files/get-new-apps-version.sh.sls
    - template: jinja
    - cwd: /var/www
    - runas: www-data
    - default:
      version: {{ new_version }}
      apps: {{ a }}
      external_apps: {{ e }}
{% endif %}
