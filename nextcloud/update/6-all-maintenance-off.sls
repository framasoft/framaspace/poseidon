{% from "framaspace/map.jinja" import hosts with context %}
{% set f           = pillar['framaspace'] %}
{% set i           = pillar['instance_to_upgrade'] %}
{% set new_version = f['nextcloud']['version'] %}

{% if salt['file.readlink']('/var/www/nc-instances/' ~ i ~ '.' ~ f['domain']) != '/var/www/nc/nextcloud-' ~ new_version ~ '/' %}
set-prod-{{ i }}-to-new-version:
  file.symlink:
    - name: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}
    - target: /var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}/
    - user: www-data
    - group: www-data
    - makedirs: True
    - force: True
symlink-for-prod-{{ i }}:
  file.symlink:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.conf.php
    - target: {{ i }}.conf.prod.php
    - user: www-data
    - group: www-data
    - makedirs: True
    - force: True

{%   if grains['id'] == f['nginx']['primary'] %}
run-cron-{{ i }}:
  cmd.run:
    - name: php cron.php
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{%   endif %}
{% else %}
notice-{{ i }}:
  cmd.run:
    - name: echo "{{ i }} est déjà en version {{ new_version }}"
{% endif %}
