{% from "framaspace/map.jinja" import hosts with context %}
{% set f        = pillar['framaspace'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% set a        = f['nextcloud']['apps'].keys() | list %}
{% set b        = f['nextcloud']['disabled-apps'].keys() | list %}
{% set apps     = a + b %}
{% set redis_s  = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
{% set i           = pillar['instance_to_upgrade'] %}
{% set data        = active[i] %}
{% set new_version = f['nextcloud']['version'] %}
{% set dbpwd       = '%s-%s' % (i, f['pg']['password-seed']) %}
{% set mpwd        = '%s-%s' % (i, f['minio']['password-seed']) %}
{% set office_type = fi['active'][i]['office_type'] %}

{% if not salt['file.directory_exists']('/var/www/nc/nextcloud-' ~ new_version ~ '-dev/') and not salt['file.directory_exists']('/var/www/nc/nextcloud-' ~ new_version) %}
/var/www/nc/:
  file.directory:
    - user: www-data
    - group: www-data
nextcloud_package:
  archive.extracted:
    - name: /var/www/nc/
    - source: https://download.nextcloud.com/server/releases/nextcloud-{{ new_version }}.tar.bz2
    - source_hash: https://download.nextcloud.com/server/releases/nextcloud-{{ new_version }}.tar.bz2.sha512
    - user: www-data
    - group: www-data
    - if_missing: /var/www/nc/nextcloud-{{ new_version }}
  file.rename:
    - name: /var/www/nc/nextcloud-{{ new_version }}-dev
    - source: /var/www/nc/nextcloud
    - makedirs: True
{% endif %}
