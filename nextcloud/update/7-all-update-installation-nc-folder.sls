{% from "framaspace/map.jinja" import hosts with context %}
{% set f       = pillar['framaspace'] %}
{% set a       = f['nextcloud']['apps'].keys() | list %}
{% set b       = f['nextcloud']['disabled-apps'].keys() | list %}
{% set apps    = a + b %}
{% set redis_s = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}

# After we updated all instances, some last steps.
## Remove old installation folder
rm-old-installation-folder:
  file.absent:
    - name: /var/www/installation/nextcloud/
## Use update folder for installation folder
mv-dev-nc-to-installation-folder:
  file.rename:
    - name: /var/www/installation/nextcloud
    - source: /var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}-dev
    - makedirs: True
## Create a minimal clean config in installation folder
installation-nc-config:
  file.managed:
    - name: /var/www/installation/nextcloud/config/config.php
    - source: salt://framaspace/nextcloud/files/nextcloud-install-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      domain: {{ pillar['framaspace']['domain'] }}
      version: {{ f['nextcloud']['version_for_config'] }}
      apps: "'{{ apps | join("', '") }}'"
      installation: True
      redis_s: "'{{ redis_s | join("', '") }}'"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
