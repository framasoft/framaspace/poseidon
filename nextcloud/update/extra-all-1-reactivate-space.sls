{% set f           = pillar['framaspace'] %}
{% set i           = pillar['instance_to_upgrade'] %}
{% set new_version = f['nextcloud']['version'] %}

{% if salt['file.readlink']('/var/www/nc-instances/' ~ i ~ '.' ~ f['domain']) != '/var/www/nc/nextcloud-' ~ new_version ~ '/' %}
enable-config-nc-{{ i }}:
  file.rename:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}
    - source: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}.disabled
    - force: True
{% else %}
notice-{{ i }}:
  cmd.run:
    - name: echo "{{ i }} est déjà en version {{ new_version }}"
{% endif %}
