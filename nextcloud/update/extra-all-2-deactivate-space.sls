{% set f           = pillar['framaspace'] %}
{% set i           = pillar['instance_to_upgrade'] %}
{% set new_version = f['nextcloud']['version'] %}

disable-config-nc-{{ i }}:
  file.rename:
    - name: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}.disabled
    - source: /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}
    - force: True
