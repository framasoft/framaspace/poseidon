#!/bin/bash

NEW_VERSION=$(grep "OC_Version = array" {{ nc_folder }}version.php | sed -e "s/.*array(//" -e "s/).*//" -e "s/,/./g")
sed -e "s/'version' => .*/'version' => '$NEW_VERSION'/" -i {{ nc_folder }}config/config.php
