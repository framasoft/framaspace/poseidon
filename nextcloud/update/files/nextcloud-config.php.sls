<?php
    // Unified configuration for upgrade
    $CONFIG = array (
        'trusted_proxies' =>
          array (
            0 => '127.0.0.1',
            1 => '::1',
        ),
        'datadirectory' => '/var/www/nc-data',
        'dbtype' => 'pgsql',
        'dbport' => '',
        'dbtableprefix' => 'oc_',
        'default_phone_region' => 'FR',
        'default_locale' => 'fr_FR',
        'default_language' => 'fr',
        'log_type' => 'syslog',
        'appsallowlist' => [{{ apps }}],
        'preview_imaginary_url' => 'https://imaginary.{{ domain }}/',
        'versions_retention_obligation' => 'auto, 32',
        'enabledPreviewProviders' =>
          array (
            0 => 'OC\\Preview\\PNG',
            1 => 'OC\\Preview\\JPEG',
            2 => 'OC\\Preview\\GIF',
            3 => 'OC\\Preview\\BMP',
            4 => 'OC\\Preview\\XBitmap',
            5 => 'OC\\Preview\\MP3',
            6 => 'OC\\Preview\\TXT',
            7 => 'OC\\Preview\\MarkDown',
            8 => 'OC\\Preview\\OpenDocument',
            9 => 'OC\\Preview\\Krita',
            10 => 'OC\\Preview\\Imaginary',
        ),
        'maintenance_window_start' => 1,
        'memcache.local' => '\\OC\\Memcache\\APCu',
        'memcache.distributed' => '\\OC\\Memcache\\Redis',
        'memcache.locking' => '\\OC\\Memcache\\Redis',
        'redis.cluster' =>
          array (
            'seeds' =>
              array (
                {{ redis_s }}
            ),
            'timeout' => 0.0,
            'read_timeout' => 0.0,
            'failover_mode' => \RedisCluster::FAILOVER_ERROR,
        ),
        'upgrade.disable-web' => true,
        'loglevel' => 0,
        'debug' => false,
        'skeletondirectory' => '/opt/demeter/skeleton',
        'templatedirectory' => '/opt/demeter/template',
        'mail_domain' => '{{ mail_domain }}',
        'mail_smtpmode' => 'sendmail',
        'mail_smtphost' => '127.0.0.1',
        'mail_smtpport' => 25,
        'mail_sendmailmode' => 'smtp',
        'has_valid_subscription' => true,
        'version'           => '{{ old_version }}',
        'dbhost'            => '127.0.0.1:5432',
        'appstoreenabled'   => false,
        'instanceid'        => '{{ instanceid }}',
        'passwordsalt'      => '{{ passwordsalt }}',
        'secret'            => '{{ secret }}',
        'trusted_domains'   =>
          array (
            0 => '{{ instance }}.{{ domain }}',
        ),
        'overwrite.cli.url' => 'https://{{ instance }}.{{ domain }}',
        'dbname'            => 'nc-{{ instance }}',
        'dbuser'            => '{{ instance }}',
        'dbpassword'        => '{{ dbpassword }}',
        'syslog_tag'        => 'nc-framaspace-{{ instance }}',
        'mail_from_address' => 'framaspace-{{ instance }}-noreply',
        'installed'         => true,
{% if pillar['framaspace']['nextcloud']['prod_settings'] is defined %}
{%   for setting, value in pillar['framaspace']['nextcloud']['prod_settings'].items() %}
        '{{ setting }}'     => '{{ value }}',
{%-   endfor %}
{%- endif %}
        'objectstore'       =>
           array (
            'class' => '\\OC\\Files\\ObjectStore\\S3',
            'arguments' =>
               array (
                'bucket'               => 'nc-{{ instance }}',
                'key'                  => 'nc-{{ instance }}',
                'secret'               => '{{ miniopassword }}',
                'objectPrefix'         => '{{ instance }}:oid:urn:',
                'hostname'             => '127.0.0.1:8042',
                'use_ssl'              => false,
                'use_path_style'       => true,
                'autocreate'           => false,
                'verify_bucket_exists' => false,
            ),
        )
    );
?>
