#!/bin/bash

curl -s -L https://apps.nextcloud.com/api/v1/platform/{{ version }}/apps.json -o /tmp/apps.json

mkdir -p /tmp/apps

{% for app, v in apps.items() | sort %}
URL=$(jq -r '.[] | select(.id | match("^{{ app }}$")) | .releases | .[] | select(.version | match("^{{ v }}$")) | .download' < /tmp/apps.json)
if [[ -n $URL ]]
then
    cd /tmp/apps/
    echo "Downloading {{ app }} version {{ v }}"
    curl -s -L $URL -o {{ app }}-{{ v }}.tar.gz
    cd /var/www/nc/nextcloud-{{ version }}-dev/apps
    rm -rf {{ app }}/
    tar xf /tmp/apps/{{ app }}-{{ v }}.tar.gz
    rm /tmp/apps/{{ app }}-{{ v }}.tar.gz
else
    cat <<EOF
=======================================================================
== Unable to find URL to download {{ app }} version {{ v }}
=======================================================================
EOF
fi
{% endfor %}

{% for app, url in external_apps.items() | sort %}
if [[ -n "{{ url }}" ]]
then
    cd /tmp/apps/
    echo "Downloading external app {{ app }}"
    curl -s -L "{{ url }}" -o {{ app }}.tar.gz
    cd /var/www/nc/nextcloud-{{ version }}-dev/apps
    rm -rf {{ app }}/
    tar xf /tmp/apps/{{ app }}.tar.gz
    rm /tmp/apps/{{ app }}.tar.gz
else
    cat <<EOF
=======================================================================
== Bad URL to download external app {{ app }}
=======================================================================
EOF
fi
{% endfor %}

cd /tmp
rm /tmp/apps.json
rmdir /tmp/apps
