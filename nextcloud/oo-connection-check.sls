{% set f      = pillar['framaspace'] %}
{% set fi     = pillar['framaspace-instances'] | default({}) %}
{% set active = fi['active'] | default({}) %}

/opt/check-onlyoffice-connection-instances.txt:
  file.managed:
    - user: www-data
    - group: www-data
    - contents:
{% for k, v in active.items() -%}
{%-   if v['office_type'] == 20 %}
      - {{ k }}{%- endif %}
{% endfor %}

/opt/check-onlyoffice-connection.sh:
  file.managed:
    - source: salt://framaspace/nextcloud/files/check-onlyoffice-connection.sh
    - user: www-data
    - group: www-data
{% if grains['id'] == f['nginx']['primary'] %}
  cron.present:
    - name: /opt/check-onlyoffice-connection.sh
    - identifier: check-onlyoffice-connection.sh
    - comment: "Check onlyoffice connection for Nextcloud instances"
    - user: www-data
    - minute: 2
    - hour: 4
{% endif %}
