demeter:
  git.latest:
    - name: https://framagit.org/framasoft/framaspace/demeter.git
    - user: www-data
    - target: /opt/demeter
  file.directory:
    - name: /opt/demeter/install-skeleton
    - user: www-data
    - group: www-data
