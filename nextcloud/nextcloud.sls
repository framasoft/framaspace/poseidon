{% from "framaspace/map.jinja" import hosts with context %}
{% set f       = pillar['framaspace'] %}
{% set a       = f['nextcloud']['apps'].keys() | list %}
{% set b       = f['nextcloud']['disabled-apps'].keys() | list %}
{% set apps    = a + b %}
{% set redis_s = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
/var/www/nc/:
  file.directory:
    - user: www-data
    - group: www-data
nextcloud_package:
  archive.extracted:
    - name: /var/www/nc/
    - source: https://download.nextcloud.com/server/releases/nextcloud-{{ f['nextcloud']['version'] }}.tar.bz2
    - source_hash: https://download.nextcloud.com/server/releases/nextcloud-{{ f['nextcloud']['version'] }}.tar.bz2.sha512
    - user: www-data
    - group: www-data
    - if_missing: /var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}
  file.rename:
    - name: /var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}
    - source: /var/www/nc/nextcloud
    - makedirs: True
/var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}/config/config.php:
  file.managed:
    - source: salt://framaspace/nextcloud/files/nextcloud-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      domain: {{ f['domain'] }}
      version: {{ f['nextcloud']['version_for_config'] }}
      apps: "'{{ apps | join("', '") }}'"
      installation: False
      redis_s: "'{{ redis_s | join("', '") }}'"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}

/var/www/config-instances:
  file.directory:
    - user: www-data
    - group: www-data

/var/www/nc-instances:
  file.directory:
    - user: www-data
    - group: www-data

/var/www/nc-data/.ocdata:
  file.managed:
    - user: www-data
    - group: www-data
    - makedirs: True

/var/www/nc/nextcloud-{{ f['nextcloud']['version'] }}/config/CAN_INSTALL:
  file.absent
