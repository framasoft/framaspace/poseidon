{% from "framaspace/map.jinja" import hosts with context %}
{% set f       = pillar['framaspace'] %}
{% set a       = f['nextcloud']['apps'].keys() | list %}
{% set b       = f['nextcloud']['disabled-apps'].keys() | list %}
{% set apps    = a + b %}
{% set redis_s = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
/var/www/installation/:
  file.directory:
    - user: www-data
    - group: www-data
nextcloud_for_install_package:
  archive.extracted:
    - name: /var/www/installation/
    - source: https://download.nextcloud.com/server/releases/nextcloud-{{ f['nextcloud']['version'] }}.tar.bz2
    - source_hash: https://download.nextcloud.com/server/releases/nextcloud-{{ f['nextcloud']['version'] }}.tar.bz2.sha512
    - user: www-data
    - group: www-data
    - if_missing: /var/www/installation/nextcloud
  file.managed:
    - name: /var/www/installation/nextcloud/config/config.php
    - source: salt://framaspace/nextcloud/files/nextcloud-install-config.php.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - default:
      domain: {{ pillar['framaspace']['domain'] }}
      version: {{ f['nextcloud']['version_for_config'] }}
      apps: "'{{ apps | join("', '") }}'"
      installation: True
      redis_s: "'{{ redis_s | join("', '") }}'"
      mail_domain: {{ f['mail_domain'] | default(f['domain']) }}
