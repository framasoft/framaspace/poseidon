#!/bin/bash

apply_patch() {
    PATCH=$1
    ALREADY_PATCHED=$(patch --dry-run --forward --strip=1 --input=$PATCH | grep -c -P 'Reversed \(or previously applied\) patch detected!  Skipping patch.|which already exists!  Skipping patch.')
    if [[ $ALREADY_PATCHED -eq 0 ]]
    then
        echo "Applying $PATCH"
        patch --backup --forward --strip=1 --input=$PATCH
    else
        echo "Skipping $PATCH as it seems to be already applied"
    fi
}

cd {{ nc_dir }}

for i in /opt/asclepios/patches/nc/*
do
    apply_patch $i
done
for app in /opt/asclepios/patches/apps/*
do
    app=$(basename $app)
    cd {{ nc_dir }}/apps/$app
    for i in /opt/asclepios/patches/apps/$app/*
    do
        apply_patch $i
    done
done

cd /opt/asclepios/hard_patches
for i in $(find . -mindepth 1 -type f | sed -e "s@^./@@")
do
    if [[ $i != '.gitkeep' ]]; then
        echo "Copying $i to {{ nc_dir }}$i"
        cp $i {{ nc_dir }}$i
    fi
done
