#!/usr/bin/python3

import psycopg2

tos_file = open('/opt/demeter/terms_of_service/terms_fr.md', 'r')

tos = tos_file.read()


conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s"
                        % ('{{ dbhost }}',
                           'nc-{{ domain }}',
                           '{{ domain }}',
                           '{{ dbpwd }}'))
cur = conn.cursor()

cur.execute("""\
ALTER TABLE oc_termsofservice_terms
ADD CONSTRAINT country_language_uniq_for_tos
    UNIQUE(country_code,language_code);""")

cur.execute("""\
INSERT INTO oc_termsofservice_terms
    (country_code, language_code, body)
    VALUES ('--', 'fr', %s)
    ON CONFLICT (country_code, language_code)
    DO
        UPDATE SET body = %s;""", (tos, tos))

cur.execute("""\
ALTER TABLE oc_termsofservice_terms
DROP CONSTRAINT country_language_uniq_for_tos;""")
conn.commit()

print(tos)
