#!/bin/bash

cd /var/www/nc-instances/ || exit 1

find /etc/php/8.2/*/conf.d/ -name 20-igbinary.ini -delete -print | grep -q . && systemctl restart php8.2-fpm

DATA_FILE=/opt/check-onlyoffice-connection-instances.txt

if [[ -e $DATA_FILE ]]; then
    while IFS= read -r NC_INSTANCE; do
        export NC_INSTANCE
        LINK=$(readlink "/var/www/config-instances/${NC_INSTANCE:0:1}/${NC_INSTANCE:0:2}/${NC_INSTANCE}.frama.space/${NC_INSTANCE}.conf.php")
        if [[ $LINK == "${NC_INSTANCE}.conf.prod.php" ]]; then
            cd "$NC_INSTANCE.frama.space" || exit 2
            php occ onlyoffice:documentserver --check |& grep -vP "Document server https://${NC_INSTANCE}\.office\.frama\.space/ version .* is successfully connected"
            cd /var/www/nc-instances/ || exit 3
        fi
    done <"$DATA_FILE"
fi
