<?php
if (isset($_SERVER['HTTP_HOST'])) {
    $instance = $_SERVER['HTTP_HOST'];
    $instance = str_replace('.{{ domain }}', '', $instance);
} else {
    $instance = getenv('NC_INSTANCE');
}
if (!isset($instance)) {
    $CONFIG = array();
} else {
    $instance_file = sprintf('/var/www/config-instances/%s/%s/%s.{{ domain }}/%s.conf.php', $instance[0], $instance[0].$instance[1], $instance, $instance);
    $CONFIG = array (
        'trusted_proxies' => ['127.0.0.1', '::1'],
        'datadirectory' => '/var/www/nc-data',
        'dbtype' => 'pgsql',
        'dbport' => '',
        'dbtableprefix' => 'oc_',
        'default_phone_region' => 'FR',
        'default_locale' => 'fr_FR',
        'default_language' => 'fr',
        'config_is_read_only' => true,
        'log_type' => 'syslog',
        'appsallowlist' => [{{ apps }}],
        'preview_imaginary_url' => 'https://imaginary.{{ domain }}/',
        'versions_retention_obligation' => 'auto, 32',
        'enabledPreviewProviders' => [
            'OC\\Preview\\PNG',
            'OC\\Preview\\JPEG',
            'OC\\Preview\\GIF',
            'OC\\Preview\\BMP',
            'OC\\Preview\\XBitmap',
            'OC\\Preview\\MP3',
            'OC\\Preview\\TXT',
            'OC\\Preview\\MarkDown',
            'OC\\Preview\\OpenDocument',
            'OC\\Preview\\Krita',
            'OC\\Preview\\Imaginary',
        ],
        'maintenance_window_start' => 1,
        'memcache.local' => '\\OC\\Memcache\\APCu',
        'memcache.distributed' => '\\OC\\Memcache\\Redis',
        'memcache.locking' => '\\OC\\Memcache\\Redis',
        'redis.cluster' => [
            'seeds' => [{{ redis_s }}],
            'timeout' => 0.0,
            'read_timeout' => 0.0,
            'failover_mode' => \RedisCluster::FAILOVER_ERROR,
        ],
        'upgrade.disable-web' => true,
        'loglevel' => 2,
        'debug' => false,
        'skeletondirectory' => '/opt/demeter/skeleton',
        'templatedirectory' => '/opt/demeter/template',
        'mail_domain' => '{{ mail_domain }}',
        'mail_smtpmode' => 'sendmail',
        'mail_smtphost' => '127.0.0.1',
        'mail_smtpport' => 25,
        'mail_sendmailmode' => 'smtp',
        'has_valid_subscription' => true,

        'version' => '{{ version }}',
    );
    if (file_exists($instance_file)) {
        include $instance_file;
    }
}
