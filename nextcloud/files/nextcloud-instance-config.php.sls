<?php
$INSTANCE = array(
    'dbhost'            => '{{ dbhost | default('127.0.0.1') }}:5432',
    'appstoreenabled'   => false,
    'instanceid'        => '{{ instanceid }}',
    'passwordsalt'      => '{{ passwordsalt }}',
    'secret'            => '{{ secret }}',
    'trusted_domains'   => array (0 => '{{ instance }}.{{ domain }}'),
    'overwrite.cli.url' => 'https://{{ instance }}.{{ domain }}',
    'dbname'            => 'nc-{{ instance }}',
    'dbuser'            => '{{ instance }}',
    'dbpassword'        => '{{ dbpassword }}',
    'syslog_tag'        => 'nc-framaspace-{{ instance }}',
    'mail_from_address' => 'framaspace-{{ instance }}-noreply',
    'overwrite.cli.url' => 'https://{{ instance }}.{{ domain }}',
    'installed'         => true,
{% if pillar['framaspace']['nextcloud']['prod_settings'] is defined %}
{%   for setting, value in pillar['framaspace']['nextcloud']['prod_settings'].items() %}
    '{{ setting }}'     => '{{ value }}',
{%-   endfor %}
{%- endif %}
{% if maintenance is defined and maintenance %}
    'maintenance'       => true,
    'loglevel'          => 0,
{%- endif %}
    'objectstore'       => array(
        'class' => '\\OC\\Files\\ObjectStore\\S3',
        'arguments' => array(
            'bucket'               => 'nc-{{ instance }}',
            'key'                  => 'nc-{{ instance }}',
            'secret'               => '{{ miniopassword }}',
            'objectPrefix'         => '{{ instance }}:oid:urn:',
            'hostname'             => '127.0.0.1:8042',
            'use_ssl'              => false,
            'use_path_style'       => true,
            'autocreate'           => false,
            'verify_bucket_exists' => false,
        ),
    )
);
$CONFIG = array_merge($CONFIG, $INSTANCE);
?>
