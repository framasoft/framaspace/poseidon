{% from "framaspace/map.jinja" import hosts with context %}
{% set f       = pillar['framaspace'] %}
{% set a       = f['nextcloud']['apps'] %}
{% set b       = f['nextcloud']['disabled-apps'] %}
{% set e       = f['nextcloud']['external-apps'] | default({}) %}
{% set version = f['nextcloud']['version'] %}

{% do a.update(b) %}

update-apps-code:
  pkg.installed:
    - pkgs:
      - curl
      - jq
  cmd.script:
    - source: salt:///framaspace/nextcloud/files/get-apps.sh.sls
    - template: jinja
    - cwd: /var/www
    - runas: www-data
    - default:
      version: {{ version }}
      apps: {{ a }}
      external_apps: {{ e }}
