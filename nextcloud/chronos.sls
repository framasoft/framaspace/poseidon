chronos-deps:
  pkg.installed:
    - pkgs:
      - python3-logbook
      - python3-psycopg2
      - python3-requests
      - python3-yaml

chronos-config:
  file.managed:
    - name: /etc/chronos.yml
    - source: salt://framaspace/nextcloud/files/chronos.yml.sls
    - template: jinja
    - user: www-data
    - group: www-data
    - mode: 600

chronos:
  git.latest:
    - name: https://framagit.org/framasoft/framaspace/chronos.git/
    - target: /opt/chronos/
  file.managed:
    - name: /etc/systemd/system/chronos.service
    - source: /opt/chronos/chronos.service
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/chronos.service
  service.running:
    - name: chronos
    - reload: False
    - enable: True
    - watch:
      - file: /etc/chronos.yml
      - git: chronos
