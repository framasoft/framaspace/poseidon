{% set f        = pillar['framaspace'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% for i, data in active.items() %}
{%   for setting in ['defFormats', 'editFormats'] %}
config-onlyoffice-{{ setting }}-{{ i }}:
  cmd.run:
    - name: JSON=$(sed -e "s/ //g" /opt/demeter/onlyoffice/{{ setting }}.json | tr -d '\n') && php occ config:app:set onlyoffice {{ setting }} --value "$JSON"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
    - onlyif:
      - test -f /var/www/config-instances/{{ i[:1] }}/{{ i[:2] }}/{{ i }}.{{ f['domain']}}/{{ i }}.installed
{%   endfor %}
{% endfor %}
