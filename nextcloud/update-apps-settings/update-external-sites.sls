{% set f      = pillar['framaspace'] %}
{% set fi     = pillar['framaspace-instances'] | default({}) %}
{% set active = fi['active']   | default({}) %}
{% for i, data in active.items() %}
# Set external sites
set-external-sites-{{ i }}:
  cmd.run:
    - name: SITES=$(cat /opt/demeter/external/sites.json | tr "\n" " " | sed -e "s@ \+@ @g" -e "s@/@\\\\/@g") && php occ config:app:set external sites --value "$SITES"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}

{% endfor %}
