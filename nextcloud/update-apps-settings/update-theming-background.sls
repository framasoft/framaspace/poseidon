{% set f      = pillar['framaspace'] %}
{% set fi     = pillar['framaspace-instances'] | default({}) %}
{% set active = fi['active']   | default({}) %}
{% for i, data in active.items() %}
# Set background
set-background-{{ i }}:
  cmd.run:
    - name: php occ theming:config background /opt/demeter/theming/background.jpg
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{% endfor %}
