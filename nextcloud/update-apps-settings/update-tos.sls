{% set f        = pillar['framaspace'] %}
{% set fi       = pillar['framaspace-instances'] | default({}) %}
{% set active   = fi['active']   | default({}) %}
{% set inactive = fi['inactive'] | default({}) %}
{% do active.update(inactive) %}
{% for i, data in active.items() %}
{%   set dbpwd = '%s-%s' % (i, f['pg']['password-seed']) %}
# Insert TOS in database
add-TOS-{{ i }}:
  cmd.script:
    - name: salt://framaspace/nextcloud/files/set_tos.py.sls
    - source: salt://framaspace/nextcloud/files/set_tos.py.sls
    - template: jinja
    - default:
      domain: {{ i }}
      dbpwd: {{ dbpwd | sha256 }}

{% endfor %}
