{% set f      = pillar['framaspace'] %}
{% set fi     = pillar['framaspace-instances'] | default({}) %}
{% set active = fi['active'] | default({}) %}

# Ensure that demeter is up to date
update-demeter:
  git.latest:
    - name: https://framagit.org/framasoft/framaspace/demeter.git
    - user: www-data
    - target: /opt/demeter

{% for i, data in active.items() %}
# Set custom CSS
set-custom-css-{{ i }}:
  cmd.run:
    - name: CSS=$(cat /opt/demeter/theming_customcss/hide-settings.css) && php occ config:app:set theming_customcss customcss --value "$CSS"
    - cwd: /var/www/nc-instances/{{ i }}.{{ f['domain'] }}/
    - runas: www-data
    - env:
      - NC_INSTANCE: {{ i }}
{% endfor %}
