{% from "framaspace/map.jinja" import hosts, miniorep with context %}
{% set volumes = [] %}
{% for pool, disks in pillar['framaspace']['minio']['rep-pools'].items() | sort %}
{%   do volumes.append('http://10.0.100.{' ~ pool  ~ '}:9000/mnt/data{' ~ disks ~ '}/minio-storage') %}
{% endfor %}
/etc/default/minio:
  file.managed:
    - mode: 600
    - contents:
      - 'MINIO_OPTS="--address {{ hosts[grains['id']]['ip'] }}:9000 --console-address {{ hosts[grains['id']]['ip'] }}:44563"'
      - 'MINIO_VOLUMES="{{ volumes | join(' ') }}"'
      - 'MINIO_ROOT_USER="{{ pillar['framaspace']['minio']['root-user'] }}"'
      - 'MINIO_ROOT_PASSWORD="{{ pillar['framaspace']['minio']['root-password'] }}"'
minio.service:
  file.managed:
    - name: /etc/systemd/system/minio.service
    - source: salt://framaspace/minio/files/minio.service
  service.running:
    - restart: True
    - enable: True
    - watch:
      - file: /etc/default/minio
      - file: /etc/systemd/system/minio.service
