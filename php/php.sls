{% from "framaspace/map.jinja" import hosts with context %}
{% set p             = pillar['framaspace']['php'] %}
{% set redis_servers = [] %}
{% for hostname, value in hosts.items()|sort %}
{%   if hostname is match('^redis') %}
{%     do redis_servers.append('seed[]=%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}
php-packages:
  pkg.installed:
    - pkgs:
      - php-fpm
      - php-apcu
      - php-curl
      - php-gd
      - php-json
      - php-xml
      - php-mbstring
      - php-zip
      - php-pgsql
      - php-intl
      - php-bcmath
      - php-gmp
      - php-redis
      - php-imagick
      - libmagickcore-6.q16-6-extra

pool-static-options:
  ini.options_present:
    - name: /etc/php/8.2/fpm/pool.d/www.conf
    - separator: '='
    - sections:
        www:
          listen: 127.0.0.1:9000
          pm: static
          pm.status_path: "'/status'"
          'env[PATH]': /usr/local/bin:/usr/bin:/bin
{% if p['pool'] is defined %}
pool-options:
  ini.options_present:
    - name: /etc/php/8.2/fpm/pool.d/www.conf
    - separator: '='
    - sections:
{%   for key, data in p['pool'].items() %}
        {{ key }}:
{%     for k, val in data.items() %}
          {{ k }}: {{ val }}
{%     endfor %}
{%   endfor %}
{% endif %}
php-ini-static-options:
  ini.options_present:
    - name: /etc/php/8.2/fpm/php.ini
    - separator: '='
    - sections:
        Session:
          session.save_handler: rediscluster
          session.save_path: '"{{ redis_servers | join('&') }}&timeout=3&read_timeout=3&failover=error"'
php-ini-cli-options:
  ini.options_present:
    - name: /etc/php/8.2/cli/php.ini
    - separator: '='
    - sections:
        apc:
          apc.enable_cli: 1
{% if p['ini'] is defined %}
php-ini-options:
  ini.options_present:
    - name: /etc/php/8.2/fpm/php.ini
    - separator: '='
    - sections:
{%   for key, data in p['ini'].items() %}
        {{ key }}:
{%     for k, val in data.items() %}
          {{ k }}: {{ val }}
{%     endfor %}
{%   endfor %}
{% endif %}
/etc/php/8.2/cli/conf.d/19-igbinary.ini:
  file.rename:
    - source: /etc/php/8.2/cli/conf.d/20-igbinary.ini
/etc/php/8.2/fpm/conf.d/19-igbinary.ini:
  file.rename:
    - source: /etc/php/8.2/fpm/conf.d/20-igbinary.ini
php8.2-fpm.service:
  service.running:
    - reload: True
    - watch:
      - ini: /etc/php/*
      - file: /etc/php/*
# We use Redis sessions, so no need for that timer
phpsessionclean.timer:
  service.dead:
    - enable: False

# When updating php, 20-igbinary.ini can reappear
remove-20-igbinary:
  cron.present:
    - name: "sleep $(echo | awk '{srand(); print rand()*3600}'); find /etc/php/8.2/*/conf.d/ -name 20-igbinary.ini -delete -print | grep -q . && systemctl restart php8.2-fpm"
    - user: root
    - minute: 3
    - hour: 3
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - identifier: "remove-20-igbinary"
    - comment: "When updating php, 20-igbinary.ini can reappear"
