{% from "framaspace/map.jinja" import hosts with context %}
{% set pg         = pillar['framaspace']['pg'] %}
{% set postgresql = namespace(value=0) %}
pgpool2:
  pkg.installed:
    - pkgs:
      - pgpool2
      - postgresql-client # Pour le debug, pour avoir psql disponible
      - libjson-xs-perl # Paquet recommandé

/root/.pcppass:
  file.managed:
    - user: root
    - group: root
    - mode: 600
    - contents: '*:*:*:{{ pg['pgpool']['pcp_root_pwd'] }}'

/etc/pgpool2/pcp.conf:
  file.managed:
    - source: salt://framaspace/php/files/pcp.conf.sls
    - template: jinja
    - mode: 640
    - user: postgres
    - group: postgres
    - default:
      pwd: {{ pg['pgpool']['pcp_root_pwd'] | md5 }}

/etc/pgpool2/pgpool.conf:
  ini.options_present:
    - separator: '='
    - sections:
        port: 5432
        pcp_listen_addresses: "'localhost'"

        log_destination: "'syslog,stderr'"
        log_standby_delay: "'if_over_threshold'"

        load_balance_mode: 'on'

        ignore_leading_white_space: 'on'

        disable_load_balance_on_write: "'trans_transaction'"
        statement_level_load_balance: 'on'

{% if 'num_init_children' not in pg['pgpool']['config'] %}
        num_init_children: 64
{% endif %}
{% if 'max_pool' not in pg['pgpool']['config'] %}
        max_pool: 32
{% endif %}
{% if 'child_max_connections' not in pg['pgpool']['config'] %}
        child_max_connections: 128
{% endif %}
        sr_check_user: "'pgpool_check'"
        sr_check_password: "'{{ pg['pgpool']['pwd'] }}'"
{% if 'delay_threshold' not in pg['pgpool']['config'] %}
        delay_threshold: 10000000
{% endif %}
        failover_command: "'/opt/warn_node_updown.sh down %h'"
        failback_command: "'/opt/warn_node_updown.sh up %h'"

        allow_clear_text_frontend_auth: 'on'

{% for key, val in pg['pgpool']['config'].items() %}
        {{ key }}: "'{{ val }}'"
{% endfor %}

        backend_hostname0: "'{{ hosts[pg['primary']]['ip'] }}'"
        backend_weight0: 8
        backend_port0: "5432"
        backend_flag0: "'ALWAYS_PRIMARY'"
{% for hostname, value in hosts.items()|sort %}
{%   if hostname is match('^postgresql') and hostname != pg['primary'] %}
{%       set postgresql.value    = postgresql.value + 1 %}
        backend_hostname{{ postgresql.value }}: "'{{ hosts[hostname]['ip'] }}'"
        backend_weight{{ postgresql.value }}: 7
        backend_application_name{{ postgresql.value }}: "'{{ hostname | replace('-', '') }}'"
        backend_port{{ postgresql.value }}: "5432"
        backend_flag{{ postgresql.value }}: "'ALLOW_TO_FAILOVER'"
{%   endif %}
{% endfor %}

pgpool2.service:
  service.running:
    - reload: True
    - watch:
      - file: /etc/pgpool2/pcp.conf
      - ini: /etc/pgpool2/pgpool.conf
