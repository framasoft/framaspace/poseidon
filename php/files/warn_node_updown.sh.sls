#!/bin/bash

SERV=$2

{% if gotify is defined and gotify['url'] is defined and gotify['tokens'] is defined -%}
alert-gotify() {
    TOKEN=$1
    TITLE=$2
    MESSAGE=$3
    PRIORITY=$4

    PAYLOAD=$(cat <<EOF
{
    "title": "[Pgpool2][$(hostname)] $TITLE",
    "message": "$MESSAGE",
    "priority": $PRIORITY
}
EOF
)

    curl -s -X POST "{{ gotify['url'] }}/message?token=$TOKEN" \
      -H  "accept: application/json" \
      -H "Content-Type: application/json" \
      -d "$PAYLOAD"
}
{%- endif %}

if [[ $1 == 'down' ]]
then
{% if emails is defined -%}
  {%- for email in emails %}
    cat <<EOF | mail -s "[pgpool2][$(hostname)] $SERV n’est plus joignable" {{ email }}
Le serveur $SERV n’est plus joignable depuis $(hostname -f) depuis $(date) !

Le service auto_attach_node.service le rattachera dès qu’il sera de nouveau
joignable, mais il faut aller voir pourquoi il n’est plus joignable.
EOF
  {%- endfor %}
{%- endif %}
{% if gotify is defined and gotify['url'] is defined and gotify['tokens'] is defined -%}
  {%- for token in gotify['tokens'] %}
    alert-gotify {{ token }} "$SERV n’est plus joignable" "Le serveur $SERV n’est plus joignable depuis $(hostname -f) depuis $(date)•!" 5
  {%- endfor %}
{%- endif %}
else
{% if emails is defined -%}
  {%- for email in emails %}
    cat <<EOF | mail -s "[pgpool2][$(hostname)] $SERV de nouveau en ligne" {{ email }}
Le serveur $SERV est de nouveau joignable depuis $(hostname -f) depuis $(date) !

Il faut quand même s’assurer de la raison de la coupure.
EOF
  {%- endfor %}
{%- endif %}
{% if gotify is defined and gotify['url'] is defined and gotify['tokens'] is defined -%}
  {%- for token in gotify['tokens'] %}
    alert-gotify {{ token }} "$SERV de nouveau en ligne" "Le serveur $SERV est de nouveau joignable depuis $(hostname -f) depuis $(date)•!" 1
  {%- endfor %}
{%- endif %}
fi
