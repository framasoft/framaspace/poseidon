#!/bin/bash

#pgpool status

#0 - This state is only used during the initialization. PCP will never display it.
#1 - Node is up. No connections yet.
#2 - Node is up. Connections are pooled.
#3 - Node is down.

export PCPPASSFILE="/root/.pcppass"
export PGPASSWORD={{ pg['pgpool']['pwd'] }}
QUIET=0
DAEMON=0
LOOP=60

function check_node() {
    NB=$1
    IP=$2

    STATUS=$(pcp_node_info -h 127.0.0.1 -U root -p 9898 -n $NB -w | cut -d " " -f 3)
    if [[ $QUIET -eq 0 ]]
    then
        TEXT="up"
        if [[ $STATUS -eq 3 ]]
        then
            TEXT="down"
        fi
        echo $(date +%Y.%m.%d-%H:%M:%S.%3N)" [INFO] NODE $NB status $STATUS ($TEXT)";
    fi

    # If server is detached
    if [[ $STATUS -eq 3 ]]
    then
        echo -n $(date +%Y.%m.%d-%H:%M:%S.%3N)" [WARN] NODE $NB is down - "
        psql -h $IP -U pgpool_check -t -c "SELECT 1;" postgres >/dev/null 2>&1
        # If server is reachable
        if [[ $? -eq 0 ]]
        then
            echo "attaching node"
            TMP=$(pcp_attach_node -h 127.0.0.1 -U root -p 9898 -n $NB -w -v)
            if [[ $QUIET -eq 0 ]]
            then
                echo $(date +%Y.%m.%d-%H:%M:%S.%3N)" [INFO] "$TMP
            fi
        else
            echo "waiting for node to be reachable"
        fi
    fi
}

function usage() {
    cat <<EOF
USAGE:
auto_attach_node.sh [-q] [-d] [-l <60>]

OPTIONS:
    -q       quiet, print only messages when a node is down
    -d       run in loop (Ctrl-C to exit)
    -l <60>  time in seconds to wait between checks (default is 60 seconds)
             only used when using -d

EOF
    exit
}
while getopts ":qdl:" option; do
    case "${option}" in
        q)
            QUIET=1
            ;;
        d)
            DAEMON=1
            ;;
        l)
            LOOP=${OPTARG}
            ;;
        ?)
            echo "Invalid option: -${OPTARG}."
            usage
            ;;
    esac
done

while true
do
    check_node 0 {{ hosts[pg['primary']]['ip'] }}
{%- set postgresql = namespace(value=0) %}
{%- for hostname, value in hosts.items()|sort -%}
{%-   if hostname is match('^postgresql') and hostname != pg['primary'] -%}
{%-       set postgresql.value = postgresql.value + 1 %}
    check_node {{ postgresql.value }} {{ hosts[hostname]['ip'] }}
{%-   endif -%}
{% endfor %}

    if [[ $DAEMON -ne 1 ]]
    then
        exit
    fi

    sleep $LOOP
done

