{% from "framaspace/map.jinja" import hosts with context %}
{% set f          = pillar['framaspace'] %}
{% set pg         = f['pg'] %}
/opt/warn_node_updown.sh:
  file.managed:
    - source: salt://framaspace/php/files/warn_node_updown.sh.sls
    - template: jinja
    - mode: 755
    - default:
      gotify: {{ pg['pgpool']['warn']['gotify'] }}
      emails: {{ pg['pgpool']['warn']['emails'] }}

/opt/pgpool_auto_attach_node.sh:
  file.managed:
    - source: salt://framaspace/php/files/auto_attach_node.sh.sls
    - template: jinja
    - mode: 700
    - user: root
    - group: root
    - default:
      hosts: {{ hosts }}
      pg: {{ pg }}

/etc/systemd/system/pgpool_auto_attach_node.service:
  file.managed:
    - source: salt://framaspace/php/files/auto_attach_node.service
  service.running:
    - name: pgpool_auto_attach_node.service
    - enable: true
    - watch:
      - file: /opt/pgpool_auto_attach_node.sh
      - file: /etc/systemd/system/pgpool_auto_attach_node.service
