# Poseidon

Poseidon is part of the [Framaspace ecosystem](https://framagit.org/framasoft/framaspace/).

Poseidon is the repository of our [Salt](https://saltproject.io/) recipes.

## Dependencies

You’ll need Salt. Go to [the Salt install guide](https://docs.saltproject.io/salt/install-guide/en/latest/) to install it

## Installation

On the same server you installed [Hermes](https://framagit.org/framasoft/framaspace/hermes) on:

```bash
cd /srv/salt
git clone https://framagit.org/framasoft/framaspace/poseidon framaspace
```

## LICENSE

© Framasoft 2023, licensed under the terms of the GNU GPLv3.
See [LICENSE](LICENSE) file
