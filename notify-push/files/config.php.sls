<?php
$CONFIG = array (
    'trusted_proxies' => ['127.0.0.1', '::1'],
    'dbtype' => 'pgsql',
    'dbport' => '',
    'dbtableprefix' => 'oc_',
    'redis.cluster' => [
        'seeds' => [{{ redis_s }}],
        'timeout' => 0.0,
        'read_timeout' => 0.0,
        'failover_mode' => 1,  #  numeric value of \RedisCluster::FAILOVER_ERROR
    ],
    'dbhost'            => 'localhost-ipv4:5432',
    'dbname'            => 'nc-{{ instance }}',
    'dbuser'            => '{{ instance }}',
    'dbpassword'        => '{{ dbpassword }}',
    'overwrite.cli.url' => 'https://{{ instance }}.{{ domain }}',
    'trusted_domains'   => array (0 => '{{ instance }}.{{ domain }}'),
);
