{% from "framaspace/map.jinja" import hosts with context %}
{% set f = pillar['framaspace'] %}
{% if 'notify_push' in f['nextcloud']['apps'] %}
{%   set n_version = f['nextcloud']['apps']['notify_push'] %}
{% else %}
{%   set n_version = f['nextcloud']['disabled-apps']['notify_push'] | default(None) %}
{% endif %}

/etc/hosts:
  file.append:
    - text: '127.0.0.1 localhost-ipv4'

{% if n_version is not none %}
notify-push:
  user.present:
    - name: notify-push
    - shell: /bin/false
    - home: /var/notify-push/
    - createhome: True
    - system: True
  archive.extracted:
    - name: /var/notify-push/app
    - source: https://github.com/nextcloud-releases/notify_push/releases/download/v{{ n_version }}/notify_push-v{{ n_version }}.tar.gz
    - skip_verify: True
    - user: notify-push
    - group: notify-push
    - makedirs: True
  file.managed:
    - name: /etc/systemd/system/notify-push@.service
    - source: salt://framaspace/notify-push/files/notify-push.service
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/notify-push@.service
{% endif %}
