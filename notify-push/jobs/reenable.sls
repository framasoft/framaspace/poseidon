{% set f = pillar['framaspace'] %}
{% if 'notify_push' in f['nextcloud']['apps'] %}
{%   set n_version = f['nextcloud']['apps']['notify_push'] %}
{% else %}
{%   set n_version = f['nextcloud']['disabled-apps']['notify_push'] | default(None) %}
{% endif %}

{% if n_version is not none %}
{%   for i, data in pillar['framaspace-jobs-create'].items() %}
{%     set hp_backend_server = data['nc_hp_backend_server'] | default('notify-push-1') %}
{%     if hp_backend_server == grains['id'] %}
reenable-{{ i }}:
  service.running:
    - name: notify-push@{{ data['nc_hp_backend'] }}.service
    - restart: True
    - enable: True
    - watch:
      - file: /var/notify-push/configs/config-{{ data['nc_hp_backend'] }}.php
      - file: /etc/systemd/system/notify-push@.service
{%     endif %}
{%   endfor %}
{% endif %}
