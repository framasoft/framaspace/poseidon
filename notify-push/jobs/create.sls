{% from "framaspace/map.jinja" import hosts with context %}
{% set f = pillar['framaspace'] %}
{% if 'notify_push' in f['nextcloud']['apps'] %}
{%   set n_version = f['nextcloud']['apps']['notify_push'] %}
{% else %}
{%   set n_version = f['nextcloud']['disabled-apps']['notify_push'] | default(None) %}
{% endif %}
{% set redis_s  = [] %}
{% for hostname, value in hosts.items() | sort %}
{%   if hostname is match('^redis') %}
{%     do redis_s.append('%s:6379' % value['ip']) %}
{%   endif %}
{% endfor %}

{% if n_version is not none %}
{%   for i, data in pillar['framaspace-jobs-create'].items() %}
{%     set hp_backend_server = data['nc_hp_backend_server'] | default('notify-push-1') %}
{%     if hp_backend_server == grains['id'] %}
{%       set dbpwd = '%s-%s' % (i, f['pg']['password-seed']) %}
create-{{ i }}:
  file.managed:
    - name: /var/notify-push/configs/config-{{ data['nc_hp_backend'] }}.php
    - source: salt://framaspace/notify-push/files/config.php.sls
    - template: jinja
    - makedirs: True
    - defaults:
      instance: {{ i }}
      domain: {{ f['domain'] }}
      dbpassword: {{ dbpwd | sha256 }}
      redis_s: "'{{ redis_s | join("', '") }}'"
  service.running:
    - name: notify-push@{{ data['nc_hp_backend'] }}.service
    - restart: True
    - enable: True
    - watch:
      - file: /var/notify-push/configs/config-{{ data['nc_hp_backend'] }}.php
      - file: /etc/systemd/system/notify-push@.service
{%     endif %}
{%   endfor %}
{% endif %}
