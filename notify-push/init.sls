include:
  - framaspace.common.wireguard
  - framaspace.common.rsyslog
  - framaspace.php.pgpool
  - framaspace.php.pgpool_scripts
  - .notify-push
  - .instances
